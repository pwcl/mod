﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnhollowerRuntimeLib;
using MelonLoader;
using System.IO;
using SonicBloom.Koreo;



namespace Pistol_Whip_Custom_Levels
{
    public class CustomPoster : MonoBehaviour
    {
        public CustomPoster(IntPtr ptr) : base(ptr) { }

        public string posterPath = ""; 
        public Il2CppSystem.Collections.Generic.List<TrackSection> currentColors = new Il2CppSystem.Collections.Generic.List<TrackSection>(0);
    }
}
