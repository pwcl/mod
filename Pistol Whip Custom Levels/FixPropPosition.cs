﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnhollowerRuntimeLib;

namespace Pistol_Whip_Custom_Levels
{
    class FixPropPosition : MonoBehaviour
    {
        public FixPropPosition(IntPtr ptr) : base(ptr) { }

        void Start()
        {
            gameObject.transform.position += new Vector3(0, 0, 42069);
        }

    }
}