﻿using MelonLoader;
using System;
using System.Runtime;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.TextCore;
using UnityEngine.UIElements;
using UnityEngine.Experimental;
using UnityEngine.Animations;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using Il2CppSystem.Collections;
using UnhollowerBaseLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Il2CppSystem.Collections.Generic;
using TMPro;
using System.IO;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players.Wwise;
using CloudheadGames.PistolWhip.MusicMapperSystem;
using UnityEngine.Audio;
using System.Reflection;
using Il2CppSystem.Reflection;
using Il2CppSystem;
using System.Threading;
using System.Drawing;
using UnhollowerRuntimeLib;
using HarmonyLib;

namespace Pistol_Whip_Custom_Levels
{
    public class CustomLevels : MelonMod
    {

        public static SongInfoUI infoUI;
        public static GameManager manager;
        public static LevelData current;
        public static int CustomLevelIndex = -1;
        public static bool fixswap = false;
        public static bool swapped = false;
        public static bool finishedLoading = false;
        public static System.Collections.Generic.List<Koreography> backup = new System.Collections.Generic.List<Koreography>(0);
        public static SongPanelUIController magic;
        public static Difficulty currentdiff = Difficulty.Easy;
        public static bool patchFire = true;
        public static Material propMat;
        public static GameObject posterPrefab;
        public static SceneDetailManager detailManager;
        public static SceneAppearanceManager appearanceManager;
        public static DifficultySelector difficultySelector;
        public static GameManager gameManager;
        public static LevelDatabase levelDatabase;
        public static LevelMetaDatabase levelMetaDatabase;
        public static int pageCount = 0;
        public static AkEvent niceEvent;
        public static AK.Wwise.State CustomState;
        public static WwiseStateReference CustomStateWwiseStateReference;
        public static CHLeaderboardKeyContainer_PW keyContainer;
        public static GameObject customAudioObject;
        public static AudioSource customAudio;
        public static MusicManager musicManager;
        public static SpawnManager spawnManager;
        public static SceneInventoryManager invManager;
        public static PlayerActionManager actionManager;
        public static Messages.BeatEvent beatEvent = new Messages.BeatEvent();
        public static Messages.NoBeatSpanBegin noBeatBegin = new Messages.NoBeatSpanBegin();
        public static Messages.NoBeatSpanEnd noBeatEnd = new Messages.NoBeatSpanEnd();
        public static int currentTracks = 0;
        static int currentBeat = 0;
        static int currentNoBeat = 0;
        static bool inNoBeat = false;
        static int currentLevelEvent = 0;
        static int currentShift = 0;
        static bool blockDefade = false;
        static bool fadeOut = false;
        static TransitionFadeTask fadeTask;
        public static Messages.GameEndEvent gameEnd = new Messages.GameEndEvent();
        public static Messages.SongComplete songComplete = new Messages.SongComplete();
        public static System.Reflection.Assembly thisAssembly;
        public static CustomPoster customPoster;
        public static string currentDestination = "";
        public static SceneSetSo customSetSo;
        public static UnityEvent closeDetails;
        public static Transform audioObjects;
        public static Transform geosetObjects;
        public static MelonLogger.Instance melonLogger;
        public static JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new StringEnumConverter()
            }
        };

        public static int currentLevelHover;
        public static int customLevel = -1;
        public static float gameLength = 0;
        public static Il2CppSystem.Collections.Generic.List<Il2CppSystem.Collections.Generic.List<Difficulty>> levelDiffs = new Il2CppSystem.Collections.Generic.List<Il2CppSystem.Collections.Generic.List<Difficulty>>(0);
        public static Il2CppSystem.Collections.Generic.List<string> levelNames = new Il2CppSystem.Collections.Generic.List<string>(0);
        public static Il2CppSystem.Collections.Generic.List<string> levelAPIs = new Il2CppSystem.Collections.Generic.List<string>(0);
        public static Il2CppSystem.Collections.Generic.List<TrackSection> currentColors = new Il2CppSystem.Collections.Generic.List<TrackSection>(0);
        public static string geosetPath = null;
        public static GeoSet loadedGeo = null;
        public static bool fullyLoadedGeo = false;
        public static AudioClip loadedSong;
        public static Koreography loadedKoreo = null;
        public static float progressPerFrame = 0;
        public static float customGameTime = 0;
        public static float progressBehind = 0;
        public static float moveSpeed = 3;
        public static float timingWindowSize = 150f;
        public static Transform allEnemies;

        public override void OnApplicationStart()
        {
            ClassInjector.RegisterTypeInIl2Cpp<FixPropPosition>();
            ClassInjector.RegisterTypeInIl2Cpp<ChangeSong>();
            ClassInjector.RegisterTypeInIl2Cpp<WakeUpStupid>();
            ClassInjector.RegisterTypeInIl2Cpp<CheekyMenuMaker>();
            ClassInjector.RegisterTypeInIl2Cpp<CustomPoster>();
            ClassInjector.RegisterTypeInIl2Cpp<keepGeo>();
            ClassInjector.RegisterTypeInIl2Cpp<PosterHelper>();

            melonLogger = LoggerInstance;
            thisAssembly = Assembly;
            harmony = HarmonyInstance;

            Messenger.Default.Register<Messages.GamePauseEvent>(new System.Action(HeyPlayerPaused));

            /*byte[] jsonDll = new byte[(int)thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.NewtonsoftJson.txt").Length];
            thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.NewtonsoftJson.txt").Read(jsonDll, 0, (int)thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.NewtonsoftJson.txt").Length);
            try {
                File.WriteAllBytes("./MelonLoader/Managed/Unity.Newtonsoft.Json.dll", jsonDll);
            } catch (System.Exception e)
            {
                System.Exception ignore = e;
            }*/

            Directory.CreateDirectory("./Custom Levels");

            /*if (File.Exists("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig/835190149.wem"))
            {
                File.Delete("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem");
                File.Copy("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig/835190149.wem", "./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem");
            }

            Directory.CreateDirectory("./Custom Levels");
            Directory.CreateDirectory("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig");
            Directory.CreateDirectory("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/trash");
            if (!File.Exists("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig/835190149.wem"))
            {
                File.Copy("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem", "./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig/835190149.wem");
            }*/

            byte[] loadingGif = new byte[(int)thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.Loading.gif").Length];
            thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.Loading.gif").Read(loadingGif, 0, (int)thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.Loading.gif").Length);
            
            if (!File.Exists("UserData/MelonStartScreen/I_Hate_Anime"))
            {
                if (!File.Exists("UserData/MelonStartScreen/Loading.gif"))
                {
                    File.WriteAllBytes("UserData/MelonStartScreen/Loading.gif", loadingGif);
                    Application.Quit();
                } else
                {
                    byte[] existingGif = File.ReadAllBytes("UserData/MelonStartScreen/Loading.gif");
                    if (loadingGif.Length != existingGif.Length)
                    {
                        File.Move("UserData/MelonStartScreen/Loading.gif", "UserData/MelonStartScreen/Old Loading.gif");
                        File.WriteAllBytes("UserData/MelonStartScreen/Loading.gif", loadingGif);
                    }
                }
            }
        }

        public override void OnApplicationQuit()
        {
            //File.Delete("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem");
            //File.Copy("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig/835190149.wem", "./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem");
        }

        static string findingLevelName;
        static bool findIndexOfLevelName(string testString)
        {
            return testString == findingLevelName;
        }

        public static bool CheckForFileChanges(System.DateTime startTime, string[] files)
        {
            foreach (string file in files)
            {
                if (File.GetLastWriteTime(file) > startTime)
                {
                    return true;
                }
            }

            return false;
        }

        [HarmonyPatch(typeof(GameManager), "OnEnable", new System.Type[0])]
        public static class gamemanagerEnable
        {
            private static void Prefix(GameManager __instance)
            {
                manager = __instance;
            }
        }


        /*
        /*[HarmonyPatch(typeof(MusicManager), "StopSong", new System.Type[0])]
        public static class MusicStop
        {
            private static void Prefix(MusicManager __instance)
            {
                customAudio.Pause();
            }
        }

        [HarmonyPatch(typeof(MusicManager), "SeekTo", new System.Type[3] { typeof(SonicBloom.Koreo.Players.Wwise.WwiseKoreographySet), typeof(AK.Wwise.State), typeof(float) })]
        public static class MusicSeek
        {
            private static void Prefix(MusicManager __instance, SonicBloom.Koreo.Players.Wwise.WwiseKoreographySet __0, AK.Wwise.State __1, float __2)
            {
                customAudio.time = __2;
            }
        }


        [HarmonyPatch(typeof(GameManager), "EndGame", new System.Type[0])]
        public static class EndGame
        {
            private static void Prefix(GameManager __instance)
            {
                customAudio.Pause();
                customAudio.clip = null;
            }
        }*/

        /*[HarmonyPatch(typeof(Messages.SceneColorEvent), "SceneColorEvent", new System.Type[5] { typeof(UnityEngine.Color), typeof(UnityEngine.Color), typeof(UnityEngine.Color), typeof(UnityEngine.Color), typeof(float) })]
        [HarmonyPatch(typeof(Messages.SceneColorEvent), "Create", new System.Type[5] { typeof(UnityEngine.Color), typeof(UnityEngine.Color), typeof(UnityEngine.Color), typeof(UnityEngine.Color), typeof(float) })]
        public static class createSceneColor
        {
            private static void Prefix(Messages.SceneColorEvent __instance, UnityEngine.Color __0, UnityEngine.Color __1, UnityEngine.Color __2, UnityEngine.Color __3, float __4)
            {
                //melonLogger.Msg("SHIFT: " + __4 + ", " + customAudio.time);
            }
        }*/

        [HarmonyPatch(typeof(ScoreManager), "SubmitScores", new System.Type[0])]
        public static class scoresDisabled
        {
            private static bool Prefix(ScoreManager __instance)
            {
                if (customLevel != -1)
                {
                    return false;
                } else
                {
                    return true;
                }
            }
        }

        [HarmonyPatch(typeof(GameManager), "OnStartButton", new System.Type[0])]
        public static class gameManagerStart
        {
            private static void Prefix(GameManager __instance)
            {
                if (customLevel != -1)
                {
                    string path = Directory.GetDirectories("./Custom Levels/")[levelNames.IndexOf(__instance.currentSceneBaseName)];
                    LevelInfo lInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(path + "/level.pw"), jsonSettings);
                    LoadLevel(__instance.currentDifficulty, __instance.levelData, __instance.map.trackData, lInfo, path);

                    customPoster.currentColors = currentColors;

                    loadedKoreo = null;
                    blockDefade = true;
                    fullyLoadedGeo = false;
                    gameProgress = 0;
                    frameTimes.Clear();
                    gameLength = lInfo.songLength;
                    progressPerFrame = Time.fixedDeltaTime / customAudio.clip.length;
                    customGameTime = 0;
                    progressBehind = 0;
                    moveSpeed = lInfo.moveSpeed;
                    timingWindowSize = lInfo.timingWindowSize;
                    SpawnManager.Instance.playerVelocity = Vector3.forward * moveSpeed;
                    RESETLEVEL();
                    customAudio.Stop();
                    loadedGeo = null;
                } else
                {
                    SpawnManager.Instance.playerVelocity = Vector3.forward * 3;
                }
            }
        }

        [HarmonyPatch(typeof(GameManager), "StartGame", new System.Type[0])]
        public static class StartGameHook
        {
            private static void Prefix(GameManager __instance)
            {
                if (__instance.playing && customLevel != -1)
                {
                    currentTracks = customLevel * 3 + (int)GameManager.Instance.currentDifficulty;
                    RESETLEVEL();
                    //Quantizer.Instance.playing = true;
                    Quantizer.levelEvents = new Il2CppSystem.Collections.Generic.List<MusicMapperEvent>(0);
                }
            }
        }

        public static void RESETLEVEL()
        {
            currentBeat = 0;
            currentNoBeat = 0;
            inNoBeat = false;
            currentLevelEvent = 0;
            customAudio.time = 0;
            customGameTime = 0;
            progressBehind = 0;
            currentShift = 0;
            customAudio.time = 0;
            playerDead = false;
            ManualEndGame.Instance.FakeSongComplete = false;

            currentColors = new Il2CppSystem.Collections.Generic.List<TrackSection>(0);

            string levelPath = Directory.GetDirectories("./Custom Levels/")[levelNames.IndexOf(GameManager.Instance.currentSceneBaseName)];
            LevelInfo lInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(levelPath + "/level.pw"), jsonSettings);
            string[] difs = new string[3] { "Easy", "Normal", "Hard" };
            string dif = difs[(int)GameManager.Instance.currentDifficulty];
            Tdata tData = JsonConvert.DeserializeObject<Tdata>(File.ReadAllText(levelPath + "/" + dif + ".pw_sec"), jsonSettings);

            foreach (DumbPoint shift in tData.colors)
            {
                TrackSection sec = new TrackSection();
                sec.colors = shift.colors;
                sec.start = Mathf.RoundToInt(shift.start * 1000000);
                sec.end = Mathf.RoundToInt(shift.end * 1000000);
                currentColors.Add(sec);
            }

            customPoster.currentColors = currentColors;
        }

        [HarmonyPatch(typeof(GameManager), "OnRetryGame", new System.Type[0])]
        public static class RetryGameHook
        {
            private static void Prefix(GameManager __instance)
            {
                if (__instance.playing && customLevel != -1)
                {
                    RESETLEVEL();
                    //Quantizer.Instance.playing = true;
                }
            }
        }

        static bool playerDead = false;
        [HarmonyPatch(typeof(GameManager), "OnPlayerDeath", new System.Type[0])]
        public static class PlayerDiedLol
        {
            private static void Prefix(GameManager __instance)
            {
                if (__instance.playing && customLevel != -1)
                {
                    customAudio.Pause();
                    playerDead = true;
                }
            }
        }

        static float globalVolume = 0;
        static float musicVolume = 0;
        [HarmonyPatch(typeof(AkSoundEngine), "SetRTPCValue", new System.Type[2] { typeof(uint), typeof(float) })]
        public static class setRPTC
        {
            private static void Prefix(AkSoundEngine __instance, uint __0, float __1)
            {
                if (__0 != null && __1 != null)
                {
                    if (__0 == 3695994288) // Global
                    {
                        globalVolume = __1;
                    }

                    if (__0 == 3891337659) // Music
                    {
                        musicVolume = __1;
                    }

                    customAudio.volume = Mathf.Lerp(musicVolume / 100, 0, 1 - (globalVolume / 100) / 2);
                }
                else
                {
                    customAudio.volume = 0.5f;
                }
            }

            private static Il2CppSystem.Exception Finalizer()
            {
                return null;
            }
        }

        static bool beatFrame = false;
        static float samplesFromBeat = 0;
        static int clipSampleRate;
        static bool canChain = false;
        static bool shotInBeat = false;
        static bool isPreview = true;
        static bool updateLast = true;
        static UnityEngine.Color lastMain;
        static UnityEngine.Color lastFog;
        static UnityEngine.Color lastGlow;
        static UnityEngine.Color lastEnemy;
        static Il2CppSystem.Collections.Generic.List<float> frameTimes = new Il2CppSystem.Collections.Generic.List<float>(0);
        static bool doframes = false;
        static int chainFollowUp = -1;
        [HarmonyPatch(typeof(GameManager), "Update", new System.Type[0])]
        public static class GameManagerUpdate
        {
            private static void Prefix(GameManager __instance)
            {
                if (__instance.playing && customLevel != -1)
                {
                    if (false && !__instance.paused)
                    {
                        //melonLogger.Msg($"SONG: {customAudio.time} PLAYER {GameObject.Find("UnityXR_VRCameraRig(Clone)").transform.position.z / 3} OFFSET {customAudio.time - GameObject.Find("UnityXR_VRCameraRig(Clone)").transform.position.z / 3}");
                        float dToffset = Time.deltaTime - Time.fixedDeltaTime;
                        //melonLogger.Msg(dToffset);
                        float speedAdjustment = -1;

                        if (Mathf.Abs(dToffset) < 1f / 1000)
                        {
                            speedAdjustment = 1 + ((Time.deltaTime / Time.fixedDeltaTime - 1) / 2); //+ (dToffset / Mathf.Abs(dToffset)) * Mathf.Pow(100000000000000000000f, Mathf.Abs(dToffset) - 0.2f);
                            customGameTime += Time.fixedDeltaTime * speedAdjustment + progressBehind / 4;
                            //melonLogger.Msg(speedAdjustment);
                        } else
                        {
                            customGameTime += Time.deltaTime;
                        }

                        //gameProgress += progressPerFrame * (1 + ((1 - (Time.deltaTime / Time.fixedDeltaTime)) / 3f) + progressBehind * 500); // * (1 + progressBehind * 250)

                        progressBehind = customAudio.time - customGameTime;
                        //melonLogger.Msg($"speed: {speedAdjustment}, behind: {progressBehind}");
                    }

                    if (__instance.paused)
                    {
                        PlayerMovementManager.Instance.SetPlayerPosition(Vector3.forward * moveSpeed * customAudio.time, Vector3.zero);
                    }


                    UIStateController.Instance._IsInLevelEndTransition_k__BackingField = false;
                    __instance.songstart = true;

                    if (fadeOut && false)
                    {
                        blockDefade = false;
                        if (customAudio.time >= 1)
                        {
                            fadeTask.UpdateOverlayLerp(1 - ((customAudio.time - 1) / 2));
                        }
                        if (customAudio.time >= 3)
                        {
                            fadeTask.UpdateOverlayLerp(0);
                            fadeOut = false;
                            fadeTask = null;
                        }
                    }

                    if (customAudio.time >= gameLength)
                    {
                        ManualEndGame.Instance.FakeSongComplete = true;
                        ManualEndGame.Instance.EndGameButtonHandler();
                        Messenger.Default.Send(songComplete);
                        Messenger.Default.Send(gameEnd);
                        __instance.EndGame();
                        __instance.ResetPlayer();
                        __instance.StopGame();
                        __instance.OnSongStop();
                        //__instance.ForceEndGame();
                        __instance.ReturnToMenu();
                        VR_CommonReferences.Instance.playerRootTransform.position = Vector3.zero;
                        melonLogger.Msg($"end game");
                    }
                    else
                    {
                        PlayerMovementManager.Instance.SetPlayerPosition(Vector3.forward * moveSpeed * customAudio.time, Vector3.zero);


                        if (isPreview)
                        {
                            customAudio.time = 0;
                            isPreview = false;
                        }
                        //Quantizer.Instance.playing = true;
                        spawnManager.playing = true;
                        __instance.gameTime = customAudio.time;


                        Koreography koreo = loadedKoreo;

                        if (koreo.Tracks[0].mEventList.Count > currentBeat && customAudio.timeSamples > koreo.Tracks[0].mEventList[currentBeat].StartSample)
                        {
                            if (inNoBeat)
                            {
                                Messenger.Default.Send(noBeatEnd);
                                inNoBeat = false;
                                currentNoBeat++;
                            }

                            Messenger.Default.Send(beatEvent);
                            currentBeat++;
                            chainFollowUp++;
                        }

                        if (!inNoBeat && koreo.Tracks[1].mEventList.Count > currentNoBeat && customAudio.timeSamples > koreo.Tracks[1].mEventList[currentNoBeat].StartSample)
                        {
                            Messenger.Default.Send(noBeatBegin);
                            inNoBeat = true;
                            //Quantizer.inNoBeatSpan = true;
                        }

                        if (koreo.Tracks[1].mEventList.Count > currentNoBeat && customAudio.timeSamples > koreo.Tracks[1].mEventList[currentNoBeat].EndSample)
                        {
                            Messenger.Default.Send(noBeatEnd);
                            inNoBeat = false;
                            currentNoBeat++;
                            //Quantizer.inNoBeatSpan = false;
                        }

                        if (koreo.Tracks[2].mEventList.Count > currentLevelEvent && customAudio.timeSamples > koreo.Tracks[2].mEventList[currentLevelEvent].StartSample)
                        {
                            koreo.Tracks[2].koreographyEventCallbacks.Invoke(koreo.Tracks[2].mEventList[currentLevelEvent]);
                            currentLevelEvent++;
                        }

                        if (chainFollowUp >= 0 && koreo.Tracks[0].mEventList.Count > chainFollowUp && customAudio.timeSamples > koreo.Tracks[0].mEventList[chainFollowUp].StartSample + (timingWindowSize / 2000 * clipSampleRate))
                        {
                            if (!shotInBeat)
                            {
                                canChain = false;
                            }

                            shotInBeat = false;
                            chainFollowUp++;
                        }

                        melonLogger.Msg($"current shift {currentShift}");
                        if (appearanceManager.activeShifts.Count == 0)
                        {
                            //appearanceManager.SetColors(new ColorData(UnityEngine.Random.ColorHSV(), UnityEngine.Random.ColorHSV(), UnityEngine.Random.ColorHSV(), UnityEngine.Random.ColorHSV()))
                        }
                        if (currentShift < currentColors.Count && customAudio.time > currentColors[currentShift].start / 1000000)
                        {

                            float start = currentColors[currentShift].start / 1000000;
                            float end = currentColors[currentShift].end / 1000000;

                            float lerp = (customAudio.time - start) / (end - start);

                            ColorData col = currentColors[currentShift].colors;

                            UnityEngine.Color main = UnityEngine.Color.Lerp(lastMain, col.mainColor, lerp);
                            UnityEngine.Color fog = UnityEngine.Color.Lerp(lastFog, col.fogColor, lerp);
                            UnityEngine.Color glow = UnityEngine.Color.Lerp(lastGlow, col.glowColor, lerp);
                            UnityEngine.Color enemy = UnityEngine.Color.Lerp(lastEnemy, col.enemyColor, lerp);


                            //appearanceManager.SetColors(new ColorData(main, fog, glow, enemy));

                            Messages.SceneColorEvent colorEvent = new Messages.SceneColorEvent(main, fog, glow, enemy, 0f);

                            Messenger.defaultMessenger.Send(colorEvent);
                        }

                        if (currentShift < currentColors.Count && customAudio.time > currentColors[currentShift].end / 1000000)
                        {
                            ColorData col = currentColors[currentShift].colors;

                            //appearanceManager.SetColors(col);

                            //melonLogger.Msg($"last main color: {lastMain.r} {lastMain.g} {lastMain.b} {lastMain.a}");

                            lastMain = col.mainColor;
                            lastFog = col.fogColor;
                            lastGlow = col.glowColor;
                            lastEnemy = col.enemyColor;

                            //melonLogger.Msg($"main color: {col.mainColor.r} {col.mainColor.g} {col.mainColor.b} {col.mainColor.a}");

                            updateLast = true;
                            currentShift++;
                        }
                    }
                }
            }

            /*private static Il2CppSystem.Exception Finalizer()
            {
                return null;
            }*/
        }

        [HarmonyPatch(typeof(PlayerMovementManager), "OnGameStart", new System.Type[0] {})]
        public static class startmove
        {
            private static void Postfix(PlayerMovementManager __instance)
            {
                if (customLevel != -1)
                {
                    __instance.StopAllCoroutines();
                    RESETLEVEL();
                }
            }
        }


        [HarmonyPatch(typeof(PlayerActionManager), "UpdateGameData", new System.Type[1] { typeof(HitDetails) })]
        public static class onenemhit
        {
            private static void Prefix(PlayerActionManager __instance, ref HitDetails __0)
            {
                if (customLevel != -1)
                {
                    melonLogger.Msg("ENEMY HIT LOL");
                    melonLogger.Msg("ENEMY accuracy: " + __0.accuracy);
                    melonLogger.Msg("ENEMY old how on beat: " + __0.howOnBeat);
                    melonLogger.Msg("ENEMY severity: " + __0.severity);
                    melonLogger.Msg("ENEMY type: " + __0.type);

                    float msOff;
                    if (__0.type != HitType.Melee)
                    {
                        //melonLogger.Msg($"current sample {customAudio.timeSamples}");
                        //melonLogger.Msg($"next sample {loadedKoreo.Tracks[0].mEventList[currentBeat].StartSample} last sample {loadedKoreo.Tracks[0].mEventList[currentBeat - 1].StartSample}");

                        int nearestBeat = 0;

                        if (currentBeat == 0)
                        {
                            nearestBeat = loadedKoreo.Tracks[0].mEventList[currentBeat].StartSample;
                        }
                        else if (currentBeat < loadedKoreo.Tracks[0].mEventList.Count)
                        {
                            if (Mathf.Abs(loadedKoreo.Tracks[0].mEventList[currentBeat - 1].StartSample - customAudio.timeSamples) < Mathf.Abs(loadedKoreo.Tracks[0].mEventList[currentBeat].StartSample - customAudio.timeSamples))
                            {
                                nearestBeat = loadedKoreo.Tracks[0].mEventList[currentBeat - 1].StartSample;
                            }
                            else
                            {
                                nearestBeat = loadedKoreo.Tracks[0].mEventList[currentBeat].StartSample;
                            }
                        }
                        else
                        {
                            nearestBeat = loadedKoreo.Tracks[0].mEventList[currentBeat - 1].StartSample;
                        }

                        //melonLogger.Msg($"samples from beat {nearestBeat}  {nearestBeat - customAudio.timeSamples}");

                        samplesFromBeat = nearestBeat - customAudio.timeSamples;

                        clipSampleRate = Mathf.RoundToInt(customAudio.clip.samples / customAudio.clip.length);
                        msOff = (Mathf.Abs(samplesFromBeat) / clipSampleRate) * 1000;

                        float msOff2 = (samplesFromBeat / clipSampleRate) * 1000;

                        //melonLogger.Msg($"msoff {msOff} msoff2 {msOff2}");

                        __0.howOnBeat = 0;
                        if (msOff <= timingWindowSize / 2)
                        {
                            __0.howOnBeat = 1;
                            shotInBeat = true;
                            canChain = true;
                        }
                        else if (canChain)
                        {
                            __0.howOnBeat = 1;
                        }
                        ScoreItem score = new ScoreItem();
                        score.accuracy = __instance.scoreItem.accuracy;
                        score.accuracyValue = 0;//__instance.scoreItem.accuracyValue;
                        score.addScore = __instance.scoreItem.addScore;
                        score.baseValue = __instance.scoreItem.baseValue;
                        score.onBeatValue = 0;
                        if (__0.howOnBeat == 1)
                        {
                            score.timing = OnBeatState.Perfect;
                        }
                        else
                        {
                            score.timing = OnBeatState.Normal;
                        }
                        __instance.scoreItem = score;
                    }

                    melonLogger.Msg("ENEMY new how on beat: " + __0.howOnBeat);
                    melonLogger.Msg("\n");
                }
            }

            private static void Postfix(PlayerActionManager __instance, ref HitDetails __0)
            {
                melonLogger.Msg($"pre-change score: {__instance.scoreItem.onBeatValue} or {__instance.scoreItem.baseValue}");
            }
        }

        //[HarmonyPatch(typeof(GameplayDatabase), "GetBeatScore", new System.Type[2] { typeof(float), typeof(OnBeatState) })]
        public static class updateGameScore
        {
            private static void Postfix(GameplayDatabase __instance, ref float __0, ref OnBeatState __1, ref int __result)
            {
                if (customLevel != -1 && false)
                {
                    float msOff = (Mathf.Abs(samplesFromBeat) / clipSampleRate) * 1000;
                    //melonLogger.Msg("get beat score ms off of beat: " + msOff);
                    if (msOff <= 75)
                    {
                        __result = 100;
                        canChain = true;
                        shotInBeat = true;
                    }
                    else if (canChain)
                    {
                        __result = 100;
                    }

                    /*//melonLogger.Msg("min beat: " + __instance.scoreData.onBeatMin);
                    //melonLogger.Msg("max beat: " + __instance.scoreData.onBeatMax);
                    //melonLogger.Msg("beat score: " + __result);
                    //melonLogger.Msg("\n");*/
                }
            }
        }



        [HarmonyPatch(typeof(GameManager), "ReturnToMenu", new System.Type[0])]
        public static class MenuReturn
        {
            private static void Prefix(GameManager __instance)
            {
                customAudio.Stop();
                if (customLevel != -1)
                {
                    //melonLogger.Msg("return to menu");
                    spawnManager.OnGameEnd();
                    spawnManager.playing = false;
                    Quantizer.Instance.playing = false;

                    Messages.ResetPlayer reset = new Messages.ResetPlayer();
                    Messages.GameEndEvent gameEnd = new Messages.GameEndEvent();
                    Messages.PlayerPositionEvent position = Messages.PlayerPositionEvent.Create(Vector3.zero, Vector3.zero);
                    Messages.SongStopEvent songStop = new Messages.SongStopEvent();
                    Messenger.Default.Send(reset);
                    Messenger.Default.Send(position);
                    Messenger.Default.Send(songStop);
                    Messenger.Default.Send(gameEnd);
                    RESETLEVEL();
                }
            }
        }


        [HarmonyPatch(typeof(GameManager), "ForceEndGame", new System.Type[0])]
        public static class GameManagerEnd
        {
            private static void Prefix(GameManager __instance)
            {
                customAudio.Stop();
                if (customLevel != -1)
                {
                    //melonLogger.Msg("end game");
                    spawnManager.OnGameEnd();
                    spawnManager.playing = false;
                    Quantizer.Instance.playing = false;

                    Messages.ResetPlayer reset = new Messages.ResetPlayer();
                    Messages.GameEndEvent gameEnd = new Messages.GameEndEvent();
                    Messages.PlayerPositionEvent position = Messages.PlayerPositionEvent.Create(Vector3.zero, Vector3.zero);
                    Messages.SongStopEvent songStop = new Messages.SongStopEvent();
                    Messenger.Default.Send(reset);
                    Messenger.Default.Send(position);
                    Messenger.Default.Send(songStop);
                    Messenger.Default.Send(gameEnd);
                    RESETLEVEL();
                }
            }
        }

        [HarmonyPatch(typeof(PauseController), "OnPauseButtonPressesOnInputController", new System.Type[0])]
        public static class pausegamehook
        {
            private static bool Prefix(PauseController __instance)
            {
                return !playerDead;
            }
        }


        [HarmonyPatch(typeof(CHLeaderboardKeyObject), "ToString", new System.Type[0])]
        public static class stringKeyObject
        {
            private static void Prefix(CHLeaderboardKeyObject __instance)
            {
                /*int index = 0;
                foreach (string name in levels)
                {
                    if (__instance.apiName.Contains(name) && !__instance.apiName.Contains(levelNames[index]))
                    {
                        __instance.apiName = levelNames[index] + "_" + __instance.apiName.Split('_').Last();
                    }
                    index++;
                }*/
                if (customLevel != -1)
                {
                    __instance.apiName = "PWMG_allScoresWillGoHereForNow;P";
                }
            }
        }

        [HarmonyPatch(typeof(CHLeaderboardKeyContainer_PW), "FillLeaderboardAPINames", new System.Type[0])]
        public static class fillLeaderBoardAPI
        {
            private static void Postfix(CHLeaderboardKeyContainer_PW __instance)
            {
                for (int i = 182; i < __instance.leaderboardKeyObjects.Count; i++)
                {
                    __instance.leaderboardKeyObjects[i].apiName = "PWMG_allScoresWillGoHereForNow;P";
                    //melonLogger.Msg(__instance.leaderboardKeyObjects[i].apiName);
                }
            }
        }

        static LevelData prechangeLevel = null;
        static string currentScene;
        static bool customLevelLoaded = false;
        [HarmonyPatch(typeof(GameManager), "SetDestinationAndUpdateUIFromDestinationString", new System.Type[3] { typeof(string), typeof(bool), typeof(bool) })]
        public static class setDest
        {
            private static void Prefix(GameManager __instance, ref string __0, bool __1, bool __2)
            {
                difficultySelector.InitDifficultyButtons();
                string leveldif = __0.Split(':')[0];
                //melonLogger.Msg($"changing destination to {leveldif}");

                if (levelAPIs.Contains("PWMG_" + leveldif))
                {
                    int level = Mathf.FloorToInt(levelAPIs.IndexOf("PWMG_" + leveldif) / 3);
                    string basedif = levelNames[level];

                    string destDif = __0.Split(':')[0].Substring(basedif.Length, __0.Split(':')[0].Length - basedif.Length);
                    string modString = __0.Split(':')[1];
                    Difficulty currentDiff = Difficulty.Normal;
                    if (destDif.Contains("_Easy"))
                    {
                        currentDiff = Difficulty.Easy;
                    }
                    else if (destDif.Contains("_Hard"))
                    {
                        currentDiff = Difficulty.Hard;
                    }
                    __instance.currentDifficulty = currentDiff;

                    //melonLogger.Msg(__0.Split(':')[0] + "  " + level);

                    if (currentScene != basedif || !levelDiffs[level].Contains(currentDiff))
                    {
                        Il2CppSystem.Collections.Generic.List<string> apiDIF = new Il2CppSystem.Collections.Generic.List<string>(0);
                        apiDIF.Add("_Easy"); apiDIF.Add(""); apiDIF.Add("_Hard");

                        //melonLogger.Msg($"current destination {__0.Split(':')[0]}");
                        //melonLogger.Msg($"current diff {levelDiffs[level].Count}");
                        //melonLogger.Msg($"current mods {modString}");
                        //melonLogger.Msg($"destination: {destDif}");

                        currentDestination = $"{basedif}{apiDIF[(int)levelDiffs[level][0]]}:{modString}";
                        __0 = currentDestination;
                        //melonLogger.Msg($"intercepted destination: {currentDestination}");

                        LevelInfo lInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(Directory.GetDirectories("./Custom Levels/")[level] + "/level.pw"), jsonSettings);

                        gameLength = lInfo.songLength;
                        customLevel = level;

                        string filePath = Directory.GetDirectories("./Custom Levels/")[level] + "/" + lInfo.songLocation.Split('/').Last();
                        MelonCoroutines.Start(LoadSetAndPreviewSong(filePath, lInfo.songLength, lInfo.previewTime));

                        string[] dif = new string[3] { "Easy", "Normal", "Hard" };
                        geosetPath = $"{Directory.GetDirectories("./Custom Levels/")[level]}/{dif[(int)levelDiffs[level][0]]}.pw_geo";
                        loadedGeo = null;
                        prechangeLevel = null;

                        currentScene = basedif;
                    }
                    customLevelLoaded = true;

                    /*difficultySelector.DifficultySelectorButtonsDisableAll();

                    Il2CppSystem.Collections.Generic.List<Transform> diffcultySelectorButtons = difficultySelector.DiffcultySelectorButtons;

                    for (int i = 0; i < diffcultySelectorButtons.Count; i++)
                    {
                        CncrgDifficultyButtonController buttonController = diffcultySelectorButtons[i].GetComponent<CncrgDifficultyButtonController>();
                        PWUIButton button = diffcultySelectorButtons[i].GetComponent<PWUIButton>();


                        if (__instance.currentDifficulty == buttonController.Difficulty)
                        {
                            button.SetAsSelected();
                        }
                        else if (levelDiffs[customLevel].Contains(buttonController.Difficulty))
                        {
                            button.SetAsEnabled();
                        }
                    }*/
                }
            }

            private static void Postfix(GameManager __instance, string __0, bool __1, bool __2)
            {
                if (customLevelLoaded)
                {
                    customLevelLoaded = false;

                    string destDif = __0.Split(':')[0].Substring(currentScene.Length, __0.Split(':')[0].Length - currentScene.Length);

                    try
                    {
                        difficultySelector.DifficultySelectorButtonsDisableAll();
                        MelonCoroutines.Start(ChangeButtons(destDif));
                    } catch (System.Exception ex)
                    {
                        //melonLogger.Warning("CAUGHT CRASH WTF");
                    }
                } else
                {
                    SetCurrentLevelToNull();
                }
            }

            private static void Postfix(GameManager __instance)
            {
                difficultySelector.DiffcultySelectorButtons[(int)__instance.currentDifficulty].GetComponent<OutlinedCncrgUIButton>().SetAsSelected();
            }

            private static Il2CppSystem.Exception Finalizer()
            {
                return null;
            }
        }

        static System.Collections.IEnumerator ChangeButtons(string destDif)
        {
            yield return null;

            Difficulty currentDiff = Difficulty.Normal;
            if (destDif.Contains("_Easy"))
            {
                currentDiff = Difficulty.Easy;
            }
            else if (destDif.Contains("_Hard"))
            {
                currentDiff = Difficulty.Hard;
            }

            difficultySelector.DifficultySelectorButtonsDisableAll();
            Il2CppSystem.Collections.Generic.List<Transform> diffcultySelectorButtons = difficultySelector.DiffcultySelectorButtons;
            for (int i = 0; i < diffcultySelectorButtons.Count; i++)
            {
                CncrgDifficultyButtonController buttonController = diffcultySelectorButtons[i].GetComponent<CncrgDifficultyButtonController>();
                PWUIButton button = diffcultySelectorButtons[i].GetComponent<PWUIButton>();

                if (levelDiffs[levelNames.IndexOf(currentScene)].Contains(buttonController.Difficulty))
                {
                    button.SetAsEnabled();
                    button.SetAsDeselected();
                }
                if (currentDiff == buttonController.Difficulty)
                {
                    button.SetAsSelected();
                }
            }

            ShowCustom();
        }

        [HarmonyPatch(typeof(SceneDetailManager), "CloseSceneDetails", new System.Type[0])]
        public static class CloseDetails
        {
            private static void Prefix(SceneDetailManager __instance)
            {
                customAudio.Stop();
            }
        }


        [HarmonyPatch(typeof(GameManager), "Pause", new System.Type[1] { typeof(float) })]
        public static class PauseGame
        {
            private static void Prefix(GameManager __instance)
            {
                if (customLevel != -1)
                {
                    customAudio.Pause();
                }
            }

            public static void Postfix(GameManager __instance)
            {
                /*frameTimes.Sort();
                //melonLogger.Msg($"min frameTime: {frameTimes[0]}");
                //melonLogger.Msg($"max frameTime: {frameTimes[frameTimes.Count - 1]}");

                float totalFrameTimes = 0;
                foreach (float time in frameTimes)
                {
                    totalFrameTimes += time;
                }
                totalFrameTimes /= frameTimes.Count;

                //melonLogger.Msg($"average frameTime: {totalFrameTimes}");*/
            }
        }

        [HarmonyPatch(typeof(GameManager), "Resume", new System.Type[0])]
        public static class ResumeGame
        {
            private static void Prefix(GameManager __instance)
            {
                if (customLevel != -1 && __instance.paused)
                {
                    customAudio.Play();
                }
            }
        }

        [HarmonyPatch(typeof(GameManager), "StartGame", new System.Type[0])]
        public static class onsongstart
        {
            private static void Postfix(GameManager __instance)
            {
                if (GameManager.Instance != null && customLevel != -1)
                {
                    customAudio.time = 0;
                    customGameTime = 0;
                    progressBehind = 0;
                    //Quantizer.Instance.playing = true;
                    //customAudio.Play();
                    __instance.paused = false;
                    GameManager.Instance.Resume();
                    UIStateController.Instance._IsInLevelEndTransition_k__BackingField = false;
                    RESETLEVEL();

                    Transform head = GameObject.Find("HeadCollision").transform.parent;
                    loadingImage = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    loadingImage.name = "RICKROLLED";
                    loadingImage.transform.parent = head;
                    loadingImage.transform.localScale = new Vector3(0.1f, -0.1f, 0.1f);
                    loadingImage.transform.position = head.position;
                    loadingImage.transform.rotation = head.rotation;
                    loadingImage.transform.localPosition = new Vector3(0, 0, 0.5f);

                    loadingImage.GetComponent<MeshRenderer>().material = new Material(Shader.Find("UI/Default"));
                    byte[] imageBytes;
                    Bitmap bitmap = new Bitmap(thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.8bitrick.png"));
                    ImageConverter converter = new ImageConverter();
                    imageBytes = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
                    Texture2D tex = new Texture2D(2, 2);
                    Il2CppImageConversionManager.LoadImage(tex, imageBytes);
                    loadingImage.GetComponent<MeshRenderer>().material.mainTexture = tex;
                }
            }
        }

        [HarmonyPatch(typeof(CHApp), "HasRequestedForcedInputFocus", new System.Type[0])]
        public static class chappForceFocus
        {
            private static void Postfix(CHApp __instance, ref bool __result)
            {
                if (customLevel != -1)
                {
                    __result = true;
                }
            }
        }

        [HarmonyPatch(typeof(UIStateController), "AllowedToPause", new System.Type[0])]
        public static class yesyoucanpause
        {
            private static bool Prefix(UIStateController __instance, ref bool __result)
            {
                if (customLevel != -1)
                {
                    __result = true;
                    __instance.ForcePauseMenuIfNotInPausedState();
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }


        [HarmonyPatch(typeof(GameManager), "StartSong", new System.Type[0])]
        public static class SongStart
        {
            private static bool Prefix(GameManager __instance)
            {
                if (customLevel != -1)
                {
                    RESETLEVEL();
                    //melonLogger.Msg("HMMMMMM");
                    __instance.paused = false;
                    __instance.playing = true;
                    GameManager.Instance.Resume();
                    customAudio.Play();
                    Messenger.Default.Send(noBeatEnd);
                    GameManager.Instance.songstart = true;
                    //Quantizer.Instance.playing = true;
                    UIStateController.Instance._IsInLevelEndTransition_k__BackingField = false;
                    //currentColors = levelColors[__instance.map];

                    WwiseKoreographySet[] koreoSets = new WwiseKoreographySet[3] { levelDatabase.easyKoreo, levelDatabase.normalKoreo, levelDatabase.hardKoreo };
                    //MusicManager.PlaySong(koreoSets[(int)GameManager.Instance.currentDifficulty], FallState, 0);
                    MusicManager.playingKoreo = __instance.map.trackData.koreography;
                    MusicManager.LoadKoreoSet(koreoSets[(int)GameManager.Instance.currentDifficulty]);
                    MusicManager.visor.LoadKoreoSet(koreoSets[(int)GameManager.Instance.currentDifficulty]);
                    MusicManager.visor.targetKoreographer.LoadKoreography(__instance.map.trackData.koreography);
                    spawnManager.OnGameStart();
                    spawnManager.OnSongStart();
                    spawnManager.PrespawnEnemies();
                    spawnManager.QueueSpawns();
                    spawnManager.playing = true;
                    updateLast = true;
                    __instance.playIntent = PlayIntent.FREEPLAY;
                    GameObject.Destroy(loadingImage);

                    if (loadedKoreo == null)
                    {
                        string levelPath = Directory.GetDirectories("./Custom Levels/")[levelNames.IndexOf(GameManager.Instance.currentSceneBaseName)];
                        LevelInfo lInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(levelPath + "/level.pw"), jsonSettings);
                        string[] difs = new string[3] { "Easy", "Normal", "Hard" };
                        string dif = difs[(int)GameManager.Instance.currentDifficulty];
                        KoreographyBaseC lKoreo = JsonConvert.DeserializeObject<KoreographyBaseC>(File.ReadAllText(levelPath + "/" + dif + ".pw_koreo"), jsonSettings);

                        __instance.map.trackData.koreography = ScriptableObject.CreateInstance("Koreography").Cast<Koreography>();
                        loadedKoreo = __instance.map.trackData.koreography;

                        loadedKoreo.name = lInfo.sceneDisplayName;
                        loadedKoreo.clipName = lInfo.songDisplayName;
                        loadedKoreo.mSampleRate = lKoreo.mSampleRate;
                        loadedKoreo.mTempoSections = new Il2CppSystem.Collections.Generic.List<TempoSectionDef>(0);
                        foreach (TempoSectionDefC temp in lKoreo.mTempoSections)
                        {
                            TempoSectionDef tempo = new TempoSectionDef();
                            tempo.beatsPerMeasure = temp.beatsPerMeasure;
                            tempo.bStartNewMeasure = temp.bStartNewMeasure;
                            tempo.DoesStartNewMeasure = temp.bStartNewMeasure;
                            tempo.samplesPerBeat = temp.samplesPerBeat;
                            tempo.startSample = temp.startSample;
                            tempo.sectionName = temp.sectionName;
                            loadedKoreo.mTempoSections.Add(tempo);
                        }

                        foreach (KoreographyTrackBaseC Tbase in lKoreo.mTracks)
                        {
                            KoreographyTrackBase track = ScriptableObject.CreateInstance("KoreographyTrack").Cast<KoreographyTrackBase>();
                            track.name = lInfo.sceneDisplayName + "_" + Tbase.mEventID;
                            track.mEventID = Tbase.mEventID;
                            track.mEventList = new Il2CppSystem.Collections.Generic.List<KoreographyEvent>(0);
                            foreach (KoreographyEventC eventC in Tbase.mEventList)
                            {
                                KoreographyEvent Kevent = new KoreographyEvent();
                                Kevent.mStartSample = eventC.mStartSample;
                                Kevent.mEndSample = eventC.mEndSample;
                                track.AddEvent(Kevent);
                            }
                            loadedKoreo.AddTrack(track);
                        }

                        Tdata tData = JsonConvert.DeserializeObject<Tdata>(File.ReadAllText(levelPath + "/" + dif + ".pw_sec"), jsonSettings);
                        __instance.map.trackData.sections.Clear();
                        ColorData colorData = new ColorData();
                        colorData.fogColor = tData.sections[0].fogColor;
                        colorData.glowColor = tData.sections[0].glowColor;
                        colorData.mainColor = tData.sections[0].mainColor;
                        TrackSection newSection2 = new TrackSection();
                        newSection2.fogColor = tData.sections[0].fogColor;
                        newSection2.glowColor = tData.sections[0].glowColor;
                        newSection2.mainColor = tData.sections[0].mainColor;
                        newSection2.enemyColor = tData.sections[0].enemyColor;
                        newSection2.customEnemyColor = tData.sections[0].customEnemyColor;
                        newSection2.colors = colorData;
                        newSection2.start = -24;
                        newSection2.end = Mathf.CeilToInt(lInfo.songLength);
                        __instance.map.trackData.sections.Add(newSection2);

                        lastMain = tData.sections[0].mainColor;
                        lastFog = tData.sections[0].fogColor;
                        lastGlow = tData.sections[0].glowColor;
                        lastEnemy = tData.sections[0].enemyColor;

                        currentColors = new Il2CppSystem.Collections.Generic.List<TrackSection>(0);

                        foreach (DumbPoint shift in tData.colors)
                        {
                            TrackSection sec = new TrackSection();
                            sec.colors = shift.colors;
                            sec.start = Mathf.RoundToInt(shift.start * 1000000);
                            sec.end = Mathf.RoundToInt(shift.end * 1000000);
                            currentColors.Add(sec);
                        }
                    }


                    return false;
                }
                else
                {
                    //melonLogger.Msg("YEEEEE");
                    return true;
                }
            }
        }

        static Transform enemyContainer;
        [HarmonyPatch(typeof(SpawnManager), "PrespawnEnemies", new System.Type[0])]
        public static class prespawningenemies
        {
            private static void Prefix(SpawnManager __instance)
            {
                if (customLevel != -1 && enemyContainer == null)
                {
                    RESETLEVEL();
                    enemyContainer = new GameObject("Condense thy enemies").transform;
                    string levelPath = Directory.GetDirectories("./Custom Levels/")[levelNames.IndexOf(GameManager.Instance.currentSceneBaseName)];
                    string[] difs = new string[3] { "Easy", "Normal", "Hard" };
                    string dif = difs[(int)GameManager.Instance.currentDifficulty];
                    BeatFile beatFile = JsonConvert.DeserializeObject<BeatFile>(File.ReadAllText(levelPath + "/" + dif + ".pw_beat"), jsonSettings);

                    __instance.data = GameManager.Instance.map.trackData;
                    TrackData trackData = SpawnManager.Instance.data;
                    LevelData levelData = GameManager.Instance.levelData;

                    trackData.beats.Clear();
                    trackData.beatTimes.Clear();

                    foreach (CustomBeat customBeat in beatFile.beatData)
                    {
                        BeatData Beat = new BeatData(customBeat.time);
                        Beat.targets = new Il2CppSystem.Collections.Generic.List<TargetData>(customBeat.targets.Count);

                        foreach (CustomTargetData customEnemy in customBeat.targets)
                        {
                            GameObject ActionContainer = new GameObject("ActionContainer");
                            ActionContainer.hideFlags = HideFlags.HideAndDontSave;

                            GameObject customENEMYDATA = new GameObject("Custom Enemy Data");
                            customENEMYDATA.hideFlags = HideFlags.HideAndDontSave;
                            customENEMYDATA.transform.parent = enemyContainer;
                            ActionContainer.transform.parent = customENEMYDATA.transform;
                            EnemySequence customENEMYSEQUENCE = customENEMYDATA.AddComponent<EnemySequence>();
                            customENEMYSEQUENCE.actions = new Il2CppSystem.Collections.Generic.List<EnemyAction>(0);
                            customENEMYSEQUENCE.autospawn = false;
                            customENEMYSEQUENCE.duration = 0;
                            customENEMYSEQUENCE.forceCheevoID = CheevoID.NONE;
                            customENEMYSEQUENCE.isThreat = false;
                            customENEMYSEQUENCE.playerActionLerp = customEnemy.playerActionLerp;
                            customENEMYSEQUENCE.localActionPoint = new WorldPoint(Vector3.zero, Quaternion.identity);
                            customENEMYSEQUENCE.actionHolder = ActionContainer;

                            TargetData ntarget = new TargetData(customENEMYSEQUENCE);
                            ntarget.distance = (TargetData.Distance)customEnemy.distance;
                            ntarget.placement = customEnemy.placement;
                            switch (levelData.enemySet)
                            {
                                case 0:
                                    ntarget.enemyType = (int)System.Enum.Parse(typeof(CustomTargetData.EnemySet1), customEnemy.type);
                                    break;

                                case 1:
                                    ntarget.enemyType = (int)System.Enum.Parse(typeof(CustomTargetData.EnemySet2), customEnemy.type);
                                    break;

                                case 2:
                                    ntarget.enemyType = (int)System.Enum.Parse(typeof(CustomTargetData.EnemySet3), customEnemy.type);
                                    break;
                            }
                            Quaternion rotation = Quaternion.identity;
                            ntarget.enemyOffset = new WorldPoint(customEnemy.enemyOffset.position, customEnemy.enemyOffset.rotation);
                            ntarget.ignoreForLevelRank = customEnemy.bonusEnemy;
                            ntarget.fireTimes = new Il2CppStructArray<float>(0);
                            ntarget.cheevoID = CheevoID.NONE;
                            ntarget.noGround = customEnemy.noGround;
                            ntarget.isShielded = customEnemy.shielded;

                            WorldPoint moveroot = new WorldPoint(Vector3.zero, Quaternion.identity);

                            foreach (CustomEnemyAction action in customEnemy.enemySequence)
                            {
                                switch (action.enemyAction)
                                {
                                    case CustomEnemyAction.ActionType.EnemyActionWait:
                                        EnemyActionWait wait = ActionContainer.AddComponent<EnemyActionWait>();
                                        wait.duration = action.properties.duration;
                                        wait.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        customENEMYSEQUENCE.duration += action.properties.duration;
                                        wait.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(wait);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionMove:
                                        EnemyActionMove move = ActionContainer.AddComponent<EnemyActionMove>();
                                        move.destination = action.properties.destination;
                                        move.localStartingPoint = new WorldPoint(moveroot.position, moveroot.rotation);
                                        move.speed = Mathf.CeilToInt(Vector3.Distance(moveroot.position, action.properties.destination.position) / action.properties.duration);
                                        move.localEndingPoint = new WorldPoint(moveroot.position + action.properties.destination.position, moveroot.rotation * action.properties.destination.rotation);
                                        moveroot = move.localEndingPoint;
                                        move.duration = action.properties.duration;
                                        move.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        customENEMYSEQUENCE.duration += action.properties.duration;
                                        move.facing = action.properties.facing;
                                        move.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(move);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionAimStart:
                                        EnemyActionAimStart aim = ActionContainer.AddComponent<EnemyActionAimStart>();
                                        aim.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        aim.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(aim);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionAimStop:
                                        EnemyActionAimStop hold = ActionContainer.AddComponent<EnemyActionAimStop>();
                                        hold.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        hold.stopFacing = action.properties.stopFacingOnExit;
                                        hold.stopLooking = action.properties.stopLookingOnExit;
                                        hold.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(hold);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionFire:
                                        EnemyActionFire fire = ActionContainer.AddComponent<EnemyActionFire>();
                                        fire.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        fire.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(fire);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionAimAndFire:
                                        EnemyActionAimAndFire aimandfire = ActionContainer.AddComponent<EnemyActionAimAndFire>();
                                        aimandfire.duration = action.properties.duration;
                                        aimandfire.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        customENEMYSEQUENCE.duration += action.properties.duration;
                                        aimandfire.fireTime = action.properties.fireTime;
                                        aimandfire.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(aimandfire);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionStopFiring:
                                        EnemyActionStopFiring stopfiring = ActionContainer.AddComponent<EnemyActionStopFiring>();
                                        stopfiring.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        stopfiring.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(stopfiring);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionDespawn:
                                        EnemyActionDespawn despawn = ActionContainer.AddComponent<EnemyActionDespawn>();
                                        despawn.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        despawn.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(despawn);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionFacePlayerStart:
                                        EnemyActionFacePlayerStart faceplayerstart = ActionContainer.AddComponent<EnemyActionFacePlayerStart>();
                                        faceplayerstart.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        faceplayerstart.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(faceplayerstart);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionFacePlayerStop:
                                        EnemyActionFacePlayerStop faceplayerstop = ActionContainer.AddComponent<EnemyActionFacePlayerStop>();
                                        faceplayerstop.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        faceplayerstop.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(faceplayerstop);
                                        break;

                                    case CustomEnemyAction.ActionType.EnemyActionAimWithoutFacing:
                                        EnemyActionAimWithoutFacing aimwithoutfacing = ActionContainer.AddComponent<EnemyActionAimWithoutFacing>();
                                        aimwithoutfacing.sequenceStartTime = customENEMYSEQUENCE.duration;
                                        aimwithoutfacing.sequence = customENEMYSEQUENCE;
                                        customENEMYSEQUENCE.actions.Add(aimwithoutfacing);
                                        break;
                                }
                            }
                            Beat.targets.Add(ntarget);
                        }

                        foreach (CustomObstacleData customObstacle in customBeat.obstacles)
                        {
                            ObstacleData obstacle = new ObstacleData();
                            obstacle.placement = (ObstacleData.Placement)customObstacle.placement;
                            obstacle.type = (ObstacleData.ObstacleType)customObstacle.type;

                            Beat.obstacles.Add(obstacle);
                        }


                        trackData.beats.Add(Beat);
                        trackData.beatTimes.Add(Beat.time);
                    }
                }
            }
        }

        [HarmonyPatch(typeof(SpawnManager), "QueueSpawns", new System.Type[0])]
        public static class afterqueueenemies
        {
            private static void Postfix(SpawnManager __instance)
            {
                if (customLevel != -1)
                {
                    RESETLEVEL();
                    TrackData trackData = GameManager.Instance.map.trackData;
                    string name = trackData.name;
                    Difficulty dif = trackData.difficulty;
                    LevelData data = trackData.level;

                    trackData.beats.Clear();
                    trackData.beatTimes.Clear();

                    GameObject.Destroy(enemyContainer.gameObject);
                    enemyContainer = null;
                }
            }
        }

        [HarmonyPatch(typeof(AK.Wwise.State), "SetValue", new System.Type[0] { })]
        public static class StateSetValue
        {
            private static bool Prefix(AK.Wwise.State __instance)
            {
                if (customLevel != -1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }


        static int activeCount = 0;
        [HarmonyPatch(typeof(SpawnManager), "Update", new System.Type[0])]
        public static class spawnUpdate
        {
            private static void Postfix(SpawnManager __instance)
            {
                if (__instance.activeSpawns.Count != activeCount)
                {
                    //melonLogger.Msg($"old count: {activeCount}, new count: {__instance.activeSpawns.Count}");
                    //activeCount = __instance.activeSpawns.Count;
                    //melonLogger.Msg($"current game time: {GameManager.Instance.gameTime}");
                    //melonLogger.Msg($"Enemy duration: {__instance.activeSpawns[__instance.activeSpawns.Count - 1].enemySequence.duration}");
                }
                //activeCount = __instance.activeSpawns.Count;
            }
        }



        /*[HarmonyPatch(typeof(AlphaDelay), "Start", new System.Type[0])]
        public static class panelstart
        {
            private static void Prefix(SongPanelUIController __instance)
            {
                if (posterPrefab == null && __instance.gameObject.GetComponent<SongPanelUIController>() != null)
                {
                    if (__instance.gameObject.GetComponent<SongPanelUIController>().BaseName == "TheFall")
                    {
                        posterPrefab = __instance.gameObject;
                    }
                }
            }
        }*/

        //[HarmonyPatch(typeof(EnemySequence), "Update")]
        public static class StopError
        {
            private static Il2CppSystem.Exception Finalizer()
            {
                return null;
            }
        }

        [HarmonyPatch(typeof(EnemySequence), "Start")]
        public static class setAutoSpawn
        {
            private static void Prefix(EnemySequence __instance)
            {
                __instance.autospawn = false;
            }
        }

        static Transform levelContainer;
        static Vector3 basePosterTransform = new Vector3(-2.25f, 1.3f, 0);
        static float shiftSize = 2.34f;
        [HarmonyPatch(typeof(PosterSelectorAssembler), "AssemblePosterSetFromLevelData", new System.Type[] { typeof(SceneSetSo)})]
        public static class setGroupActive
        {
            private static void Postfix(PosterSelectorAssembler __instance, SceneSetSo __0)
            {
                //melonLogger.Msg(__0.name);
                if (__0 != null && __0.name == "Custom Levels")
                {
                    //melonLogger.Msg("looking good");
                    Transform posterContainer = __instance.ContentCanvas.GetChild(2);
                    //hen melonLogger.Msg(posterContainer.name);
                    levelContainer = posterContainer;

                    MelonCoroutines.Start(LoadedPosters(posterContainer));

                    if (Mathf.FloorToInt((Directory.GetDirectories("./Custom Levels/").Length - 1) / 10) != 0)
                    {
                        customPosterScroll = GameObject.Instantiate(detailManager.transform.GetChild(10));
                        customPosterScroll.SetParent(levelContainer.parent);
                        customPosterScroll.gameObject.SetActive(true);
                        customPosterScroll.transform.position = new Vector3(2.914f, 1.925f, 5.039f);
                        customPosterScroll.name = "CUSTOM POSTER SCROLL GOES BRRRRR";
                        // 0 0 3
                        customPosterScroll.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event = new UnityEvent();
                        customPosterScroll.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(scrollPostersUp));
                        customPosterScroll.GetChild(1).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event = new UnityEvent();
                        customPosterScroll.GetChild(1).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(scrollPostersDown));

                        customPosterScroll.GetChild(3).GetComponent<TextMeshProUGUI>().text = $"1-{(Directory.GetDirectories("./Custom Levels/").Length >= 10 ? 10 : Directory.GetDirectories("./Custom Levels/").Length)} / {Directory.GetDirectories("./Custom Levels/").Length}";
                    }
                } else
                {
                    if (customPosterScroll != null)
                    {
                        GameObject.Destroy(GameObject.Find("CUSTOM POSTER SCROLL GOES BRRRRR"));
                        posterPage = 0;
                        //levelContainer.transform.localPosition = basePosterTransform;
                    }
                }
            }
        }

        static string lastPoster = "";
        static System.Collections.IEnumerator LoadedPosters(Transform posterContainer)
        {
            maxPage = Mathf.FloorToInt((Directory.GetDirectories("./Custom Levels/").Length - 1) / 10);

            while (posterContainer.transform.childCount < levelNames.Count) {
                if (posterContainer.transform.childCount >= 0 && lastPoster != posterContainer.GetChild(posterContainer.transform.childCount - 1).name) {
                    lastPoster = posterContainer.GetChild(posterContainer.transform.childCount - 1).name;
                }

                yield return null;
            }

            for (int i = 0; i < posterContainer.transform.childCount; i++)
            {
                if (i >= 10)
                {
                    //posterContainer.GetChild(i).gameObject.GetComponentInChildren<UnityEngine.UI.Image>().enabled = false;
                    //posterContainer.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        static void scrollPostersUp()
        {
            /*for (int i = posterPage * 10; i < (posterPage + 1) * 10; i++)
            {
                melonLogger.Msg(i);
                //levelContainer.GetChild(i).gameObject.GetComponentInChildren<UnityEngine.UI.Image>().enabled = false;
                levelContainer.GetChild(i).gameObject.SetActive(false);

                if (i == levelContainer.childCount)
                {
                    break;
                }
            }*/

            for (int i = 0; i < levelContainer.childCount; i++)
            {
                levelContainer.GetChild(i).gameObject.SetActive(false);
            }

            posterPage--;
            if (posterPage < 0)
            {
                posterPage = maxPage;
            }

            for (int i = posterPage * 10; i < (posterPage + 1) * 10; i++)
            {
                melonLogger.Msg(i);
                //levelContainer.GetChild(i).gameObject.GetComponentInChildren<UnityEngine.UI.Image>().enabled = true;
                levelContainer.GetChild(i).gameObject.SetActive(true);

                if (i == levelContainer.childCount)
                {
                    break;
                }
            }

            //levelContainer.transform.localPosition = basePosterTransform + Vector3.up * shiftSize * posterPage;

            customPosterScroll.GetChild(3).GetComponent<TextMeshProUGUI>().text = $"{posterPage * 10 + 1}-{(Directory.GetDirectories("./Custom Levels/").Length >= (posterPage + 1) * 10 ? (posterPage + 1) * 10 : Directory.GetDirectories("./Custom Levels/").Length)} / {Directory.GetDirectories("./Custom Levels/").Length}";
        }

        static void scrollPostersDown()
        {
            /*for (int i = posterPage * 10; i < (posterPage + 1) * 10; i++)
            {
                melonLogger.Msg(i);
                //levelContainer.GetChild(i).gameObject.GetComponentInChildren<UnityEngine.UI.Image>().enabled = false;
                levelContainer.GetChild(i).gameObject.SetActive(false);

                if (i == levelContainer.childCount)
                {
                    break;
                }
            }*/

            for (int i = 0; i < levelContainer.childCount; i++)
            {
                levelContainer.GetChild(i).gameObject.SetActive(false);
            }

            posterPage++;
            if (posterPage > maxPage)
            {
                posterPage = 0;
            }

            for (int i = posterPage * 10; i < (posterPage + 1) * 10; i++)
            {
                melonLogger.Msg(i);
                //levelContainer.GetChild(i).gameObject.GetComponentInChildren<UnityEngine.UI.Image>().enabled = true;
                levelContainer.GetChild(i).gameObject.SetActive(true);


                if (i == levelContainer.childCount)
                {
                    break;
                }
            }

            //levelContainer.transform.localPosition = basePosterTransform + Vector3.up * shiftSize * posterPage;

            customPosterScroll.GetChild(3).GetComponent<TextMeshProUGUI>().text = $"{posterPage * 10 + 1}-{(Directory.GetDirectories("./Custom Levels/").Length >= (posterPage + 1) * 10 ? (posterPage + 1) * 10 : Directory.GetDirectories("./Custom Levels/").Length)} / {Directory.GetDirectories("./Custom Levels/").Length}";
        }

        [HarmonyPatch(typeof(SceneInventoryManager), "Start", new System.Type[0])]
        public static class inventorymanager
        {

            private static void Postfix(SceneInventoryManager __instance)
            {
                /*GameObject whatthefuck = new GameObject("OfficialCustoms");
                TextLevelLoading.CustomLevelLoader loading = whatthefuck.AddComponent<TextLevelLoading.CustomLevelLoader>();
                loading.CustomSongSaveLocation = "Custom Levels";
                loading.ScanFileSystemAndImport();*/

                MelonCoroutines.Start(CalculateFrameTime());
                customAudioObject = new GameObject("customAudio");
                customAudio = customAudioObject.AddComponent<AudioSource>();

                //melonLogger.Msg("running postfix");
                GameObject CustomContent = new GameObject("Custom Content");
                CustomContent.AddComponent<WakeUpStupid>();
                //melonLogger.Msg("Wake up lol");

                posterPrefab = __instance.featureInventory.feature[0].ContentPrefab.GetComponent<PosterSelectorAssembler>().PosterPrefab;
                SongPanelUIController spC = posterPrefab.GetComponent<SongPanelUIController>();
                CustomState = new AK.Wwise.State();
                var refs = Resources.FindObjectsOfTypeAll<WwiseStateReference>();
                foreach (WwiseStateReference reference in refs)
                {
                    if (reference.objectName == "Custom")
                    {
                        CustomState.ObjectReference = reference;
                        CustomStateWwiseStateReference = reference;
                    }
                }

                allEnemies = new GameObject("ALL ENEMIES").transform;
                detailManager = Resources.FindObjectsOfTypeAll<SceneDetailManager>()[0];
                difficultySelector = Resources.FindObjectsOfTypeAll<DifficultySelector>()[2];
                gameManager = GameManager.Instance;
                customPoster = gameManager.gameObject.AddComponent<CustomPoster>();
                levelDatabase = Resources.FindObjectsOfTypeAll<LevelDatabase>()[0];
                levelMetaDatabase = Resources.FindObjectsOfTypeAll<LevelMetaDatabase>()[0];
                musicManager = Resources.FindObjectsOfTypeAll<MusicManager>()[0];
                spawnManager = Resources.FindObjectsOfTypeAll<SpawnManager>()[0];
                actionManager = Resources.FindObjectsOfTypeAll<PlayerActionManager>()[0];
                appearanceManager = Resources.FindObjectsOfTypeAll<SceneAppearanceManager>()[0];
                invManager = __instance;
                MelonCoroutines.Start(PatchVariablesAndMethods());

                /*SceneSetGroupSo newCustoms = ScriptableObject.CreateInstance<SceneSetGroupSo>();
                newCustoms.featureType = FeatureType.Training;
                newCustoms.setGroupButtonUII2LTerm = "mai-top-customs";
                newCustoms.enabled = true;
                newCustoms.name = "Customs";
                newCustoms.setGroupButtonUIText = "CUSTOMS";
                newCustoms.ContentPrefab = CustomContent;
                newCustoms.FeatureColorSet = __instance.featureInventory.feature[4].FeatureColorSet;
                newCustoms.GroupColorSet = __instance.featureInventory.feature[4].GroupColorSet;
                newCustoms.selectedStateTextColor = new UnityEngine.Color(120f / 255f, 30f / 255f, 190f / 255f, 1);
                UICustomization newCustomization = new UICustomization();
                newCustoms.Customization = __instance.featureInventory.feature[4].Customization;
                //newCustoms.Customization.FeaturePosterBackgroundSpriteColor = new UnityEngine.Color(120f / 255f, 30f / 255f, 190f / 255f, 1);
                /*(int i = 0; i < 1; i++)
                {
                    SceneSetSo basicSet = newCustoms.sceneSetGroups[0];
                    basicSet.CanonicalUIName = "LMFAO" + i;   
                    basicSet.setLabelUIText = "All";
                    basicSet.hasHeroPoster = false;
                    basicSet.heroIndex = i;
                    basicSet.setDescriptionCopy = "";
                    basicSet.blockCopy1 = "";
                    basicSet.blockCopy2 = "";
                    basicSet.name = "Test Thing" + i;
                }*/
                SceneSetSo basicSet = ScriptableObject.CreateInstance<SceneSetSo>();
                basicSet.CanonicalUIName = "Custom Levels";
                basicSet.setLabelUIText = "Custom Levels";
                basicSet.hasHeroPoster = false;
                basicSet.heroIndex = 0;
                basicSet.setDescriptionCopy = "";
                basicSet.blockCopy1 = "";
                basicSet.blockCopy2 = "";
                basicSet.blockCopy1I2LTerm = "";
                basicSet.blockCopy2I2LTerm = "";
                basicSet.name = "Custom Levels";
                basicSet.sceneBaseNameStrings = new Il2CppSystem.Collections.Generic.List<string>(0);
                basicSet.scenes = new Il2CppSystem.Collections.Generic.List<LevelData>(0);
                basicSet.playlist = ScriptableObject.CreateInstance("PlayList").Cast<PlayList>();
                basicSet.playlist.CanonicalName = "By setting this name it means custom levels are canon now, right?";
                basicSet.playlist.playList = new Il2CppSystem.Collections.Generic.List<PlayList.LevelInfo>(0);
                basicSet.playlist.name = "Customs Playlist";
                for (int i = 0; i < Directory.GetDirectories("./Custom Levels/").Length; i++)
                {
                    LoadLevelInfo(i, basicSet, false);
                }

                Messages.CustomSongImportComplete wtf = new Messages.CustomSongImportComplete();
                wtf.sceneSetSo = basicSet;
                Messenger.Default.Send(wtf);
                //newCustoms.sceneSetGroups.Add(basicSet);

                foreach (string name in levelNames)
                {
                    melonLogger.Msg(name);
                }
                //melonLogger.Msg($"buttons: {__instance.featureButtons.Count}");

                //__instance.featureInventory.feature[4] = newCustoms;
                //__instance.featureButtons[4].name = "CUSTOMS";
                //__instance.featureButtons[4].label.text = "CUSTOMS";
                //__instance.gameObject.AddComponent<CheekyMenuMaker>();
                //__instance.gameObject.name = "SceneInventoryLMAO";
                //__instance.featureButtons[4].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(ClickCustoms));



                /*__instance.featureButtons[0].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(StopMusic));
                __instance.featureButtons[0].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(SetCurrentLevelToNull));
                __instance.featureButtons[0].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(customsVisibleSet));
                __instance.featureButtons[1].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(StopMusic));
                __instance.featureButtons[1].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(SetCurrentLevelToNull));
                __instance.featureButtons[1].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(customsVisibleSet));
                __instance.featureButtons[2].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(StopMusic));
                __instance.featureButtons[2].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(SetCurrentLevelToNull));
                __instance.featureButtons[2].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(customsVisibleSet));
                __instance.featureButtons[3].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(StopMusic));
                __instance.featureButtons[3].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(SetCurrentLevelToNull));
                __instance.featureButtons[3].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(customsVisibleSet));
                __instance.featureButtons[5].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(StopMusic));
                __instance.featureButtons[5].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(SetCurrentLevelToNull));
                __instance.featureButtons[5].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(customsVisibleSet));

                __instance.featureButtons[4].gameObject.transform.GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(ShowCustom));*/
                //__instance.featureButtons[4].gameObject.transform.GetChild(0).GetChild(0).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(GrabPoster));

                //detailManager.gameObject.transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(StopMusic));
                //closeDetails = detailManager.gameObject.transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event;

                //melonLogger.Msg("ASDDSASDA");

                /*ModeHintSo customLogoHint = GameObject.Find("Mode Hints").GetComponent<ModeHintController>().HintSet.Hints[3];
                customLogoHint.title = "Customs";
                customLogoHint.copy = "Haha custom\nPistol Whip\nscenes go\nbrrrrrrrrrrrrrrr";

                melonLogger.Msg("done");*/

                finishedLoading = true;

            }
        }

        public static void LoadLevelInfo(int i, SceneSetSo basicSet, bool refresh)
        {
            string levelPath = Directory.GetDirectories("./Custom Levels/")[i];
            levelDiffs.Add(new Il2CppSystem.Collections.Generic.List<Difficulty>(0));
            LevelInfo lInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(levelPath + "/level.pw"), jsonSettings);

            Difficulty standDif = Difficulty.Easy;

            LevelData levelData = ScriptableObject.CreateInstance("LevelData").Cast<LevelData>();

            levelData.name = lInfo.sceneDisplayName;
            //levelData.description = lInfo.description + "\nMapped by " + lInfo.mapper;
            levelData.isTutorial = false;
            levelData.enemySet = (int)lInfo.enemySet;
            levelData.materialPropertiesSet = (int)lInfo.materialPropertiesSet;
            levelData.obstacleSet = (int)lInfo.obstacleSet;
            levelData.moveMode = PlayerMovementManager.MovementMode.Moving;
            //levelData.sceneDisplayName = lInfo.sceneDisplayName;
            levelData.songLength = lInfo.songLength;
            //levelData.songName = lInfo.songDisplayName;
            levelData.name = lInfo.sceneDisplayName;
            levelData.songSwitch = CustomState;
            //levelData.descI2LTerm = "sce-desc-custom-" + lInfo.sceneDisplayName;
            menuColor colors = lInfo.customMainMenuColor;
            levelData.UseCustomMainMenuColor = true;
            levelData.Main = colors.Main;
            levelData.Fog = colors.Fog;
            levelData.Glow = colors.Glow;
            levelData.Enemy = colors.Enemy;


            Il2CppSystem.Collections.Generic.List<int> difMissing = new Il2CppSystem.Collections.Generic.List<int>(0);
            if (lInfo.maps.Contains("Easy"))
            {
                if (!refresh)
                {
                    levelDiffs[i].Add(Difficulty.Easy);
                }
                else
                {
                    if (!levelDiffs[i].Contains(Difficulty.Easy))
                    {
                        levelDiffs[i].Add(Difficulty.Easy);
                    }
                }
                standDif = Difficulty.Easy;
            }
            else
            {
                difMissing.Add(0);
            }
            if (lInfo.maps.Contains("Normal"))
            {
                if (!refresh)
                {
                    levelDiffs[i].Add(Difficulty.Normal);
                }
                else
                {
                    if (!levelDiffs[i].Contains(Difficulty.Normal))
                    {
                        levelDiffs[i].Add(Difficulty.Normal);
                    }
                }

                standDif = Difficulty.Normal;
            }
            else
            {
                difMissing.Add(1);
            }
            if (lInfo.maps.Contains("Hard"))
            {
                if (!refresh)
                {
                    levelDiffs[i].Add(Difficulty.Hard);
                }
                else
                {
                    if (!levelDiffs[i].Contains(Difficulty.Hard))
                    {
                        levelDiffs[i].Add(Difficulty.Hard);
                    }
                }
                standDif = Difficulty.Hard;
            }
            else
            {
                difMissing.Add(2);
            }

            string diffString = "";
            int[] stubs = new int[3] { 0, 1, 2 };

            foreach (int thisDiff in stubs)
            {
                diffString = thisDiff == 0 ? "_Easy" : thisDiff == 1 ? "" : "_Hard";

                TrackData trackData = ScriptableObject.CreateInstance("TrackData").Cast<TrackData>();
                trackData.sampleRate = 44100;
                trackData.playerSpeed = 3;
                trackData.name = lInfo.sceneDisplayName + diffString;
                trackData.difficulty = standDif;
                trackData.level = levelData;


                GameMap map = new GameMap();
                map.trackData = trackData;
                map.data = levelData;
                map.geoSetReference = new GeoSetReference("0b60313ccc168e846b26016cd0842b26");
                levelData.maps[thisDiff] = map;

                levelDatabase.geoSetDict.Add(map.geoSetReference, map);
                levelDatabase.trackDict.Add(trackData, map);
            }

            byte[] imageBytes;
            if (File.Exists(Directory.GetDirectories("./Custom Levels/")[i] + "/poster.png"))
            {
                imageBytes = File.ReadAllBytes(Directory.GetDirectories("./Custom Levels/")[i] + "/poster.png");
            }
            else
            {
                Bitmap bitmap = new Bitmap(thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.missing.png"));
                ImageConverter converter = new ImageConverter();
                imageBytes = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
            }
            Texture2D tex = new Texture2D(2, 2);
            Il2CppImageConversionManager.LoadImage(tex, imageBytes);
            Sprite posterSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            posterSprite.name = lInfo.sceneDisplayName;

            LevelMetaDatabase.LevelMetadata customLevelMetadata = new LevelMetaDatabase.LevelMetadata();
            if (refresh)
            {
                customLevelMetadata = levelMetaDatabase.albumMetadataDict[levelData];
            }

            customLevelMetadata.art = posterSprite;
            customLevelMetadata.artIsWIP = true;
            customLevelMetadata.curatedStyles = new Il2CppSystem.Collections.Generic.List<StyleSO>(0);
            customLevelMetadata.descI2LTer = "";
            customLevelMetadata.description = lInfo.description + "\nMapped by " + lInfo.mapper;
            customLevelMetadata.DetailPosterSprite = posterSprite;
            customLevelMetadata.levelData = levelData;
            customLevelMetadata.sceneDisplayName = lInfo.songDisplayName;
            customLevelMetadata.songArtists = lInfo.songArtist;
            customLevelMetadata.songDisplayName = lInfo.songDisplayName;
            customLevelMetadata.tempo = Mathf.FloorToInt(lInfo.tempo);
            customLevelMetadata.WidePosterSprite = posterSprite;

            if (!refresh)
            {
                basicSet.sceneBaseNameStrings.Add(lInfo.sceneDisplayName);
                basicSet.scenes.Add(levelData);
                levelDatabase.levelData.Add(levelData);
                levelMetaDatabase.albumMetadata.Add(customLevelMetadata);
                levelMetaDatabase.albumMetadataDict.Add(levelData, customLevelMetadata);
                levelDatabase.lastReleasedScene = levelData;

                levelAPIs.Add("PWMG_" + lInfo.sceneDisplayName + "_Easy");
                levelAPIs.Add("PWMG_" + lInfo.sceneDisplayName);
                levelAPIs.Add("PWMG_" + lInfo.sceneDisplayName + "_Hard");
                levelNames.Add(lInfo.sceneDisplayName);
            }
        }


        public static void LoadLevel(Difficulty difficulty, LevelData levelData, TrackData trackData, LevelInfo lInfo, string levelPath)
        {
            string diffString = "";
            int thisDiff = (int)difficulty;
            string dif = "";

            switch (difficulty)
            {
                case Difficulty.Easy:
                    dif = "Easy";
                    diffString = "_Easy";
                    break;

                case Difficulty.Normal:
                    dif = "Normal";
                    diffString = "";
                    break;

                case Difficulty.Hard:
                    dif = "Hard";
                    diffString = "_Hard";
                    break;
            }

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            settings.ObjectCreationHandling = ObjectCreationHandling.Replace;


            trackData = ScriptableObject.CreateInstance("TrackData").Cast<TrackData>();

            trackData.sampleRate = 44100;
            trackData.playerSpeed = 3;
            trackData.name = lInfo.sceneDisplayName + diffString;
            trackData.difficulty = difficulty;
            trackData.level = levelData;


            // GEOSET



            /*if (geosetObjects == null)
            {
                geosetObjects = new GameObject("keep geosets loaded").transform;
            }
            GameObject keepLoaded = new GameObject(levelPath + diffString);
            keepLoaded.transform.SetParent(geosetObjects);
            keepGeo geo = keepLoaded.AddComponent<keepGeo>();
            geo.geo = geoSet;
            //customPoster.geos.Add(geoSet);
            GameObject.DontDestroyOnLoad(geoSet);*/


            // Koreography and Music Map
            KoreographyBaseC lKoreo = JsonConvert.DeserializeObject<KoreographyBaseC>(File.ReadAllText(levelPath + "/" + dif + ".pw_koreo"), jsonSettings);

            trackData.koreography = ScriptableObject.CreateInstance("Koreography").Cast<Koreography>();

            Koreography koreo = trackData.koreography;
            koreo.name = lInfo.sceneDisplayName + diffString;
            koreo.clipName = lInfo.songDisplayName;
            koreo.mSampleRate = lKoreo.mSampleRate;
            koreo.mTempoSections = new Il2CppSystem.Collections.Generic.List<TempoSectionDef>(0);
            foreach (TempoSectionDefC temp in lKoreo.mTempoSections)
            {
                TempoSectionDef tempo = new TempoSectionDef();
                tempo.beatsPerMeasure = temp.beatsPerMeasure;
                tempo.bStartNewMeasure = temp.bStartNewMeasure;
                tempo.DoesStartNewMeasure = temp.bStartNewMeasure;
                tempo.samplesPerBeat = temp.samplesPerBeat;
                tempo.startSample = temp.startSample;
                tempo.sectionName = temp.sectionName;
                koreo.mTempoSections.Add(tempo);
            }

            MusicMap musicMap = ScriptableObject.CreateInstance("MusicMap").Cast<MusicMap>();
            musicMap.name = lInfo.sceneDisplayName + diffString;
            musicMap.SampleRate = lKoreo.mSampleRate;
            musicMap.SerializedTracks = new Il2CppSystem.Collections.Generic.List<MusicMap.KeyEventListPairs>(0);
            musicMap.Tracks = new Il2CppSystem.Collections.Generic.Dictionary<string, Il2CppSystem.Collections.Generic.List<MusicMapperEvent>>(0);
            Il2CppSystem.Collections.Generic.List<MusicMapperEvent> mapBeats = new Il2CppSystem.Collections.Generic.List<MusicMapperEvent>(0);
            Il2CppSystem.Collections.Generic.List<MusicMapperEvent> mapNoBeats = new Il2CppSystem.Collections.Generic.List<MusicMapperEvent>(0);
            Il2CppSystem.Collections.Generic.List<MusicMapperEvent> mapEvents = new Il2CppSystem.Collections.Generic.List<MusicMapperEvent>(0);

            foreach (KoreographyTrackBaseC Tbase in lKoreo.mTracks)
            {
                KoreographyTrackBase track = ScriptableObject.CreateInstance("KoreographyTrack").Cast<KoreographyTrackBase>();
                track.name = lInfo.sceneDisplayName + diffString + "_" + Tbase.mEventID;
                track.mEventID = Tbase.mEventID;
                track.mEventList = new Il2CppSystem.Collections.Generic.List<KoreographyEvent>(0);
                foreach (KoreographyEventC eventC in Tbase.mEventList)
                {
                    KoreographyEvent Kevent = new KoreographyEvent();
                    Kevent.mStartSample = eventC.mStartSample;
                    Kevent.mEndSample = eventC.mEndSample;
                    track.AddEvent(Kevent);

                    MusicMapperEvent mapperEvent = new MusicMapperEvent();
                    mapperEvent.StartSample = eventC.mStartSample;
                    mapperEvent.EndSample = eventC.mEndSample;
                    switch (Tbase.mEventID)
                    {
                        case "Beat":
                            mapBeats.Add(mapperEvent);
                            break;

                        case "NoBeat":
                            mapNoBeats.Add(mapperEvent);
                            break;

                        case "Event":
                            mapEvents.Add(mapperEvent);
                            break;
                    }
                }
                koreo.AddTrack(track);
            }
            musicMap.Tracks.Add("Beat", mapBeats);
            musicMap.Tracks.Add("NoBeat", mapNoBeats);
            musicMap.Tracks.Add("Event", mapEvents);

            trackData.koreography = koreo;
            trackData.musicMap = musicMap;

            switch (diffString)
            {
                case "_Easy":
                    WwiseKoreoMediaIDEntry Eentry = new WwiseKoreoMediaIDEntry();
                    Eentry.koreo = koreo;
                    break;

                case "":
                    WwiseKoreoMediaIDEntry entry = new WwiseKoreoMediaIDEntry();
                    entry.koreo = koreo;
                    break;

                case "_Hard":
                    WwiseKoreoMediaIDEntry Hentry = new WwiseKoreoMediaIDEntry();
                    Hentry.koreo = koreo;
                    break;
            }


            // Sections and Color Shifts
            Tdata tData = JsonConvert.DeserializeObject<Tdata>(File.ReadAllText(levelPath + "/" + dif + ".pw_sec"), jsonSettings);
            trackData.sections.Clear();
            ColorData colorData = new ColorData();
            colorData.fogColor = tData.sections[0].fogColor;
            colorData.glowColor = tData.sections[0].glowColor;
            colorData.mainColor = tData.sections[0].mainColor;
            TrackSection newSection2 = new TrackSection();
            newSection2.fogColor = tData.sections[0].fogColor;
            newSection2.glowColor = tData.sections[0].glowColor;
            newSection2.mainColor = tData.sections[0].mainColor;
            newSection2.enemyColor = tData.sections[0].enemyColor;
            newSection2.customEnemyColor = tData.sections[0].customEnemyColor;
            newSection2.colors = colorData;
            newSection2.start = -24;
            newSection2.end = Mathf.CeilToInt(lInfo.songLength);
            trackData.sections.Add(newSection2);

            Il2CppSystem.Collections.Generic.List<TrackSection> shifts = new Il2CppSystem.Collections.Generic.List<TrackSection>(0);


            foreach (DumbPoint shift in tData.colors)
            {
                TrackSection sec = new TrackSection();
                sec.colors = shift.colors;
                sec.start = Mathf.RoundToInt(shift.start * 1000000);
                sec.end = Mathf.RoundToInt(shift.end * 1000000);
                shifts.Add(sec);
            }
        }

        [HarmonyPatch(typeof(SceneInventoryManager), "AssembleTraining", new System.Type[0])]
        public static class assembleCustoms
        {
            private static bool Prefix(SceneInventoryManager __instance)
            {
                if (customsVisible)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        static int findDiff(int diff, int[] diffs)
        {
            for (int i = 0; i < diffs.Length; i++)
            {
                if (diff == diffs[i])
                {
                    return i;
                }
            }
            return -1;
        }

        static bool fixLeaderboard = true;
        static void ClickCustoms()
        {
            if (!customsVisible)
            {
                manager.playIntent = PlayIntent.FREEPLAY;

                if (fixLeaderboard)
                {
                    keyContainer = Resources.FindObjectsOfTypeAll<CHLeaderboardKeyContainer_PW>()[0];
                    keyContainer.FillLeaderboardAPINames();

                    fixLeaderboard = false;
                }
            }
        }

        static void StopMusic()
        {
            AkSoundEngine.SetState("MusicState", "None");
            customPoster.posterPath = "";
            currentDestination = "";
        }

        static void FallDefault()
        {
            File.Delete("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem");
            File.Copy("./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/orig/835190149.wem", "./Pistol Whip_Data/StreamingAssets/Audio/GeneratedSoundBanks/Windows/835190149.wem");
        }

        static void SetCurrentLevelToNull()
        {
            currentLevelHover = -1;
            customLevel = -1;
            gameLength = 0;
            customAudio.Stop();

            if (allPatched)
            {
                //MelonCoroutines.Start(UnPatchVariablesAndMethods());
            }
        }

        static void customsVisibleSet()
        {
            customsVisible = false;
        }

        static bool customsVisible = false;
        public static Transform customPosterScroll;
        public static int posterPage = 0;
        public static int maxPage = 0;
        public static void LoadPosters()
        {
            posterPage = 0;
            maxPage = Mathf.FloorToInt(Directory.GetDirectories("./Custom Levels/").Length / 10);
            Transform container = GameObject.Find("Custom Content(Clone)").transform;

            for (int i = 0; i <= Mathf.FloorToInt(Directory.GetDirectories("./Custom Levels/").Length / 10); i++)
            {
                GameObject CCContainer = new GameObject($"Container {i}");
                CCContainer.transform.parent = container.transform;
                RectTransform CCRT = CCContainer.AddComponent<RectTransform>();
                CCRT.rect.Set(0, -2.6f, 4.5f, 2.6f);
                CCRT.localPosition = new Vector3(0.69f, 0, 0);
                GridLayoutGroup CCGLG = CCContainer.AddComponent<GridLayoutGroup>();
                CCGLG.cellSize = new Vector2(0.7f, 1.1f);
                CCGLG.spacing = new Vector2(0.1f, 0.1f);
                CCGLG.m_CellSize = new Vector2(0.7f, 1.1f);
                CCGLG.m_Spacing = new Vector2(0.1f, 0.1f);
                CCGLG.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
                CCGLG.constraintCount = 5;
                CCGLG.m_Constraint = GridLayoutGroup.Constraint.FixedColumnCount;
                CCGLG.m_ConstraintCount = 5;
                CCGLG.startCorner = GridLayoutGroup.Corner.UpperLeft;
                CCGLG.startAxis = GridLayoutGroup.Axis.Horizontal;
                CCGLG.m_StartCorner = GridLayoutGroup.Corner.UpperLeft;
                CCGLG.m_StartAxis = GridLayoutGroup.Axis.Horizontal;
                CCGLG.childAlignment = TextAnchor.MiddleCenter;
                CCGLG.m_ChildAlignment = TextAnchor.MiddleCenter;
                CCGLG.padding = new RectOffset(0, 0, 0, 0);
                CCGLG.m_TotalMinSize = new Vector2(0.7f, 2.3f);
                CCGLG.m_TotalPreferredSize = new Vector2(3.3f, 2.3f);
                CCGLG.m_TotalFlexibleSize = new Vector2(-1, -1);
                CCGLG.m_Rect = CCRT;
                CCGLG.useGUILayout = true;
                CCContainer.SetActive(i > 0 ? false : true);
            }

            for (int i = 0; i < Directory.GetDirectories("./Custom Levels/").Length; i++)
            {
                GameObject panel = GameObject.Instantiate(posterPrefab);
                ChangeSong changeSong = panel.AddComponent<ChangeSong>();
                changeSong.playableDiffs = levelDiffs[i].ToArray();
                panel.name = "custom " + i;
                byte[] imageBytes;
                if (File.Exists(Directory.GetDirectories("./Custom Levels/")[i] + "/poster.png"))
                {
                    imageBytes = File.ReadAllBytes(Directory.GetDirectories("./Custom Levels/")[i] + "/poster.png");
                }
                else
                {
                    Bitmap bitmap = new Bitmap(thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.missing.png"));
                    ImageConverter converter = new ImageConverter();
                    imageBytes = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
                }
                Texture2D tex = new Texture2D(2, 2);
                Il2CppImageConversionManager.LoadImage(tex, imageBytes);
                Sprite posterSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                panel.transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Image>().sprite = posterSprite;
                changeSong.songImage = posterSprite;
                changeSong.levelIndex = i;
                //panel.GetComponent<CHUI_ImgLinkButton>().selectionIconImg[0].sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

                LevelInfo lInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(Directory.GetDirectories("./Custom Levels/")[i] + "/level.pw"), jsonSettings);
                changeSong.previewTime = lInfo.previewTime;
                changeSong.songLength = lInfo.songLength;
                changeSong.niceApiNames = new string[3] {"PWMG_" + lInfo.sceneDisplayName + "_Easy",
                                                         "PWMG_" + lInfo.sceneDisplayName,
                                                         "PWMG_" + lInfo.sceneDisplayName + "_Hard"};
                SongPanelUIController uIController = panel.GetComponent<SongPanelUIController>();
                uIController.BaseName = lInfo.sceneDisplayName;
                uIController.songImage.sprite = posterSprite;
                uIController.PosterIndex = i;
                manager.CurrentGroupSceneNames.Add(lInfo.sceneDisplayName);

                Component.Destroy(panel.GetComponent<AlphaDelay>());

                CHUI_ImgLinkButton linkButton = panel.GetComponent<CHUI_ImgLinkButton>();
                linkButton.hasHoverSprite = true;
                linkButton.hoverSprite = posterSprite;
                linkButton.gameEventTrigger = new GameEventTrigger();

                panel.transform.GetChild(1).GetComponent<BoxCollider>().enabled = true;

                panel.transform.SetParent(container.GetChild(Mathf.FloorToInt(i / 10)), false);
            }


            customPosterScroll = GameObject.Instantiate(detailManager.transform.GetChild(10));
            customPosterScroll.SetParent(container);
            customPosterScroll.gameObject.SetActive(true);
            customPosterScroll.transform.position = new Vector3(2.914f, 1.925f, 5.039f);
            // 0 0 3
            customPosterScroll.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event = new UnityEvent();
            customPosterScroll.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(scrollPostersUp));
            customPosterScroll.GetChild(1).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event = new UnityEvent();
            customPosterScroll.GetChild(1).GetChild(0).GetChild(3).GetComponent<UnityEventTrigger>().Event.AddListener(new System.Action(scrollPostersDown));

            customPosterScroll.GetChild(3).GetComponent<TextMeshProUGUI>().text = $"1-{(Directory.GetDirectories("./Custom Levels/").Length >= 10 ? 10 : Directory.GetDirectories("./Custom Levels/").Length)} / {Directory.GetDirectories("./Custom Levels/").Length}";
        }

        static void ShowCustom()
        {
            /*if (!customsVisible)
            {
                //melonLogger.Msg("yeet");
                manager.CurrentGroupSceneNames.Clear();
                detailManager.DefaultPage = 0;
                pageCount = Directory.GetDirectories("./Custom Levels/").Length;

                customsVisible = true;

            }*/

            customLevel = -2;

            if (!allPatched)
            {
                MelonCoroutines.Start(PatchVariablesAndMethods());
            }
        }

        public static System.Collections.IEnumerator CalculateFrameTime()
        {
            //averageDT = Time.fixedDeltaTime;
            yield return null;
            /*Il2CppSystem.Collections.Generic.List<float> times = new Il2CppSystem.Collections.Generic.List<float>(0);
            for (int i = 0; i < 500; i++)
            {
                times.Add(Time.fixedDeltaTime);
                yield return null;
            }

            float totalFrameTimes = 0;
            foreach (float time in times)
            {
                totalFrameTimes += time;
            }
            totalFrameTimes /= times.Count;
            averageDT = totalFrameTimes;
            //melonLogger.Msg(averageDT);*/
        }

        public static System.Collections.IEnumerator LoadSetAndPreviewSong(string path, float length, float time)
        {
            if (File.Exists(path))
            {
                if (audioObjects == null)
                {
                    audioObjects = new GameObject("Keep songs loaded").transform;
                }
                //melonLogger.Msg(File.Exists(Path.Combine(Il2CppSystem.Environment.CurrentDirectory, path.Remove(0, 2))));
                var songRequest = UnityWebRequest.Get($"file://{Path.Combine(Il2CppSystem.Environment.CurrentDirectory, path.Remove(0, 2))}");
                songRequest.SendWebRequest();

                while (!songRequest.isDone) yield return null;

                AudioClip clip = WebRequestWWW.InternalCreateAudioClipUsingDH(songRequest.downloadHandler, songRequest.url, false, false, AudioType.UNKNOWN);
                clip.LoadAudioData();
                GameObject.DontDestroyOnLoad(clip);

                customAudio.clip = clip;
                customAudio.time = time;
                customAudio.Play();
                isPreview = true;
                loadedSong = clip;
            }
            else
            {
                AudioClip clip = AudioClip.Create("", Mathf.RoundToInt(44100 * length), 1, 44100, false);

                customAudio.clip = clip;
                customAudio.time = time;
                customAudio.Play();
                isPreview = true;
                loadedSong = clip;
            }
        }

        public static System.Collections.IEnumerator LoadSong(string path)
        {
            if (audioObjects == null)
            {
                audioObjects = new GameObject("Keep songs loaded").transform;
            }
            //melonLogger.Msg(File.Exists(Path.Combine(Il2CppSystem.Environment.CurrentDirectory, path.Remove(0, 2))));
            var songRequest = UnityWebRequest.Get($"file://{Path.Combine(Il2CppSystem.Environment.CurrentDirectory, path.Remove(0, 2))}");
            songRequest.SendWebRequest();

            while (!songRequest.isDone) yield return null;

            AudioClip clip = WebRequestWWW.InternalCreateAudioClipUsingDH(songRequest.downloadHandler, songRequest.url, false, false, AudioType.UNKNOWN);
            clip.LoadAudioData();
            GameObject.DontDestroyOnLoad(clip);
            GameObject keepLoaded = new GameObject(path);
            keepLoaded.transform.SetParent(audioObjects);
            AudioSource source = keepLoaded.AddComponent<AudioSource>();
            source.clip = clip;
            //customPoster.songs.Add(clip);
        }


        [HarmonyPatch(typeof(Gun), "Fire", new System.Type[0])]
        public static class gunfire
        {
            private static bool Prefix(Gun __instance)
            {
                if (patchFire)
                {
                    patchFire = false;
                    try
                    {
                        __instance.Fire();
                    }
                    catch (System.NullReferenceException e)
                    {
                        System.NullReferenceException ignore = e;
                        //melonLogger.Msg("" + e);
                    }

                    /*if (customLevel >= 0 && GameManager.Instance.playing)
                    {
                        doframes = !doframes;
                        //melonLogger.Msg("FRAMES");
                    }*/

                    return false;
                }
                else
                {
                    patchFire = true;
                    return true;
                }
            }
        }



        /*[HarmonyPatch(typeof(DitherOverlayTask), "CreateAndRun", new System.Type[8] { typeof(float), typeof(float), typeof(float), typeof(ScriptableObjectArchitecture.FloatVariable), typeof(UnityEngine.Color), typeof(ScriptableObjectArchitecture.ColorVariable), typeof(Il2CppSystem.Action), typeof(Il2CppSystem.Action)})]
        public static class ditherOverlyTaskUpdate
        {
            private static void Prefix(DitherOverlayTask __instance, float p_fadeToTime, ref float p_holdTime, float p_fadeFromTime, ScriptableObjectArchitecture.FloatVariable p_fadeOverlayOpacityVar, UnityEngine.Color p_fadeToColor, ScriptableObjectArchitecture.ColorVariable p_fadeOverlayColorVar, Il2CppSystem.Action p_callbackOnFullyFadedTo, Il2CppSystem.Action p_autoCallbackOnFullyFadedFrom)
            {
                //melonLogger.Msg($"FADE HOLD TIME {p_holdTime}");
                
            }

            public static void Postfix(DitherOverlayTask __instance, ref TransitionFadeTask __result)
            {
                //melonLogger.Msg($"FADE TASK {__result.holdTime}");
                fadeTask = __result;
            }
        }

        [HarmonyPatch(typeof(TransitionFadeTask), "UpdateOverlayLerp", new System.Type[1] { typeof(float) })]
        public static class transitionfadeupdate
        {
            private static bool Prefix(TransitionFadeTask __instance, ref float __0)
            {
                if (blockDefade)
                {
                    //melonLogger.Msg($"DE {__0}, time {customAudio.time}");
                    //return false;
                }

                return true;
            }
        }

        [HarmonyPatch(typeof(LevelManager), "SpawnLevelGeo", new System.Type[3] { typeof(GameMap), typeof(LevelAssetDatabase), typeof(int) })]
        public static class spawningLevelGeo
        {
            private static bool Prefix(LevelManager __instance)
            {
                if (customLevel != -1 && !fullyLoadedGeo)
                {
                    //melonLogger.Msg("dont load geo");
                    MelonCoroutines.Start(LoadGeosetAsync());
                    //GameManager.Instance.Pause(0.0000000000000000000000000000000000000000000001f);
                    return false;
                }

                return true;
            }

            public static void Postfix(LevelManager __instance, ref bool __result)
            {
                if (customLevel != -1)
                {
                    __result = true;
                }
            }
        }*/

        static int findChunk(int id)
        {
            for (int i = 0; i < loadedGeo.chunkSlices.Count; i++)
            {
                if (loadedGeo.chunkSlices[i].z == id)
                {
                    return i;
                }
            }

            return -1;
        }

        static void HeyPlayerPaused()
        {
            if (customLevel != -1 && playerDead)
            {
                Messages.GameUnpauseEvent unpause = new Messages.GameUnpauseEvent();
                Messenger.Default.Send(unpause);
            }
        }

        [HarmonyPatch(typeof(LevelManager), "AddCollider", new System.Type[2] { typeof(GameObject), typeof(ChunkMeshData) })]
        public static class addingCollider
        {
            private static void Prefix(LevelManager __instance, ref GameObject __0, ref ChunkMeshData __1)
            {
                if (customLevel != -1)
                {
                    Il2CppSystem.Collections.Generic.List<Vector3> verts = new Il2CppSystem.Collections.Generic.List<Vector3>(0);
                    __0.GetComponent<MeshFilter>().mesh.GetVertices(verts);
                    Il2CppSystem.Collections.Generic.List<int> tris = new Il2CppSystem.Collections.Generic.List<int>(0);
                    __0.GetComponent<MeshFilter>().mesh.GetTriangles(tris, 0);

                    __0.GetComponent<MeshFilter>().mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

                    ChunkMeshData data = loadedGeo.chunkData[findChunk(Mathf.RoundToInt(__0.transform.position.z / 16))];

                    __0.GetComponent<MeshFilter>().mesh.vertices = data.verts;
                    __0.GetComponent<MeshFilter>().mesh.triangles = data.tris;
                }
            }
        }

        static bool fullyOpaque = false;
        static bool startPlaying = false;
        static GameObject loadingImage;

        [HarmonyPatch(typeof(TransitionFadeTask), "UpdateOverlayLerp", new System.Type[1] { typeof(float) })]
        public static class transitionfadeupdate
        {
            private static void Prefix(TransitionFadeTask __instance, ref float __0)
            {
                if (customLevel != -1 && customGameTime > gameLength)
                {
                    if (__0 > 0.75f && !fullyOpaque)
                    {
                        fullyOpaque = true;

                        Transform head = GameObject.Find("HeadCollision").transform.parent;
                        loadingImage = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        loadingImage.name = "RICKROLLED";
                        loadingImage.transform.parent = head;
                        loadingImage.transform.localScale = new Vector3(0.1f, -0.1f, 0.1f);
                        loadingImage.transform.position = head.position;
                        loadingImage.transform.rotation = head.rotation;
                        loadingImage.transform.localPosition = new Vector3(0, 0, 0.5f);

                        loadingImage.GetComponent<MeshRenderer>().material = new Material(Shader.Find("UI/Default"));
                        byte[] imageBytes;
                        Bitmap bitmap = new Bitmap(thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.8bitrick.png"));
                        ImageConverter converter = new ImageConverter();
                        imageBytes = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
                        Texture2D tex = new Texture2D(2, 2);
                        Il2CppImageConversionManager.LoadImage(tex, imageBytes);
                        loadingImage.GetComponent<MeshRenderer>().material.mainTexture = tex;
                    }

                    if (__0 < 0.75f && fullyOpaque)
                    {
                        GameObject.Destroy(loadingImage);
                        loadedGeo = null;
                        MelonCoroutines.Start(UnloadStuff());
                        fullyOpaque = false;
                        startPlaying = true;
                        customAudio.time = 0;
                        customGameTime = 0;
                    }

                    if (__0 < 0.5f && startPlaying)
                    {
                        
                        startPlaying = false;

                        if (GameObject.Find("RICKROLLED") != null)
                        {
                            GameObject.Destroy(GameObject.Find("RICKROLLED"));
                        }
                    }
                }
            }
        }

        public static System.Collections.IEnumerator UnloadStuff()
        {
            melonLogger.Msg("unloading");
            var clean = Resources.UnloadUnusedAssets();

            while (!clean.isDone) yield return null;
            melonLogger.Msg("unloaded");
        }

        public static System.Collections.IEnumerator LoadGeosetAsync()
        {
            //melonLogger.Msg("LOADING GEO");
            customAudio.Stop();

            Geoset geoData = JsonConvert.DeserializeObject<Geoset>(File.ReadAllText(geosetPath), jsonSettings);
            loadedGeo = ScriptableObject.CreateInstance("GeoSet").Cast<GeoSet>();

            loadedGeo.chunkSize = new Vector3i(32, 32, 32);
            loadedGeo.decoratorCubes = new Il2CppSystem.Collections.Generic.List<OscillatingObjectData>(0);
            loadedGeo.staticProps = new Il2CppSystem.Collections.Generic.List<WorldObject>(0);
            loadedGeo.dynamicProps = new Il2CppSystem.Collections.Generic.List<WorldObject>(0);
            loadedGeo.scale = 0.5f;


            // CHUNKS
            for (int k = 0; k < geoData.chunks.Length; k++)
            {
                ChunkMeshData chunk = new ChunkMeshData();
                chunk.tris = geoData.chunks[k].tris;
                chunk.verts = geoData.chunks[k].verts;
                Vector3Int vec = geoData.chunks[k].id;
                chunk.id = new Vector3i(vec.x, vec.y, vec.z);
                loadedGeo.chunkData.Add(chunk);

                yield return null;
            }

            // CHUNK SLICES
            for (int k = 0; k < geoData.chunksForSlices.Length; k++)
            {
                CombineInstance[] sliceParts = new CombineInstance[geoData.chunksForSlices[k].chunks.Length];
                for (int m = 0; m < geoData.chunksForSlices[k].chunks.Length; m++)
                {
                    GameObject filterObject = new GameObject("filter Object");
                    MeshFilter filter = GameObject.Instantiate(filterObject).AddComponent<MeshFilter>();
                    filter.mesh = new Mesh();
                    Chunk chunk = geoData.chunks[geoData.chunksForSlices[k].chunks[m]];
                    filter.mesh.vertices = chunk.verts;
                    filter.mesh.triangles = chunk.tris;
                    filter.transform.position = new Vector3(chunk.id.x * 16, chunk.id.y * 16, 0);
                    sliceParts[m].mesh = filter.mesh;
                    sliceParts[m].transform = filter.transform.localToWorldMatrix;
                    GameObject.Destroy(filter.gameObject);
                    GameObject.Destroy(filterObject);

                    yield return null;
                }
                GameObject combining = new GameObject("combining");
                MeshFilter finishedfilter = GameObject.Instantiate(combining).AddComponent<MeshFilter>();
                finishedfilter.mesh = new Mesh();
                finishedfilter.mesh.CombineMeshes(sliceParts);

                ChunkMeshSlice slice = new ChunkMeshSlice();
                slice.z = geoData.chunksForSlices[k].id;
                slice.verts = finishedfilter.mesh.vertices;
                slice.tris = finishedfilter.mesh.triangles;
                loadedGeo.chunkSlices.Add(slice);
                GameObject.Destroy(finishedfilter.gameObject);
                GameObject.Destroy(combining);

                yield return null;
            }

            // DECORATOR CUBES
            loadedGeo.decoratorCubes = new Il2CppSystem.Collections.Generic.List<OscillatingObjectData>(0);
            foreach (OscillatingObjectData dat in geoData.decCubes)
            {
                loadedGeo.decoratorCubes.Add(dat);

                yield return null;
            }

            LevelManager.SpawnLevelGeo(GameManager.Instance.map, Resources.FindObjectsOfTypeAll<LevelAssetDatabase>()[0]);

            fullyLoadedGeo = true;
            //melonLogger.Msg("LOADED GEO");
            GameManager.Instance.Resume();
            customAudio.Play();
            customAudio.time = 0;
            fadeOut = true;
        }

        public static class GameMap_geoSet
        {
            public static void Postfix(GameMap __instance, ref GeoSet __result)
            {
                if (customLevel != -1)
                {
                    customAudio.time = 0;
                    customGameTime = 0;
                    progressBehind = 0;

                    //melonLogger.Msg("Get geoset");

                    if (loadedGeo == null)
                    {
                        /*loadedGeo = ScriptableObject.CreateInstance("GeoSet").Cast<GeoSet>();
                        loadedGeo.chunkSize = new Vector3i(32, 32, 32);
                        loadedGeo.decoratorCubes = new Il2CppSystem.Collections.Generic.List<OscillatingObjectData>(0);
                        loadedGeo.staticProps = new Il2CppSystem.Collections.Generic.List<WorldObject>(0);
                        loadedGeo.dynamicProps = new Il2CppSystem.Collections.Generic.List<WorldObject>(0);
                        loadedGeo.scale = 0.5f;
                        loadedGeo.track = GameManager.Instance.map.trackData;

                        loadedGeo.chunkData.Add(new ChunkMeshData());*/

                        Geoset geoData = JsonConvert.DeserializeObject<Geoset>(File.ReadAllText(geosetPath), jsonSettings);
                        loadedGeo = ScriptableObject.CreateInstance("GeoSet").Cast<GeoSet>();

                        loadedGeo.chunkSize = new Vector3i(32, 32, 32);
                        loadedGeo.decoratorCubes = new Il2CppSystem.Collections.Generic.List<OscillatingObjectData>(0);
                        loadedGeo.staticProps = new Il2CppSystem.Collections.Generic.List<WorldObject>(0);
                        loadedGeo.dynamicProps = new Il2CppSystem.Collections.Generic.List<WorldObject>(0);
                        loadedGeo.scale = 0.5f;


                        // CHUNKS
                        for (int k = 0; k < geoData.chunks.Length; k++)
                        {
                            ChunkMeshData chunk = new ChunkMeshData();
                            chunk.verts = geoData.chunks[k].verts;
                            chunk.tris = geoData.chunks[k].tris;
                            Vector3Int vec = geoData.chunks[k].id;
                            chunk.id = new Vector3i(vec.x, vec.y, vec.z);
                            loadedGeo.chunkData.Add(chunk);

                            ChunkMeshSlice slice = new ChunkMeshSlice();
                            slice.z = vec.z;
                            slice.verts = geoData.chunks[k].verts;
                            slice.tris = geoData.chunks[k].tris;
                            loadedGeo.chunkSlices.Add(slice);
                        }

                        // CHUNK SLICES
                        /*for (int k = 0; k < geoData.chunksForSlices.Length; k++)
                        {
                            CombineInstance[] sliceParts = new CombineInstance[geoData.chunksForSlices[k].chunks.Length];
                            for (int m = 0; m < geoData.chunksForSlices[k].chunks.Length; m++)
                            {
                                GameObject filterObject = new GameObject("filter Object");
                                MeshFilter filter = GameObject.Instantiate(filterObject).AddComponent<MeshFilter>();
                                filter.mesh = new Mesh();
                                Chunk chunk = geoData.chunks[geoData.chunksForSlices[k].chunks[m]];
                                filter.mesh.vertices = chunk.verts;
                                filter.mesh.triangles = chunk.tris;
                                filter.transform.position = new Vector3(chunk.id.x * 16, chunk.id.y * 16, 0);
                                sliceParts[m].mesh = filter.mesh;
                                sliceParts[m].transform = filter.transform.localToWorldMatrix;
                                GameObject.Destroy(filter.gameObject);
                                GameObject.Destroy(filterObject);
                            }
                            GameObject combining = new GameObject("combining");
                            MeshFilter finishedfilter = GameObject.Instantiate(combining).AddComponent<MeshFilter>();
                            finishedfilter.mesh = new Mesh();
                            finishedfilter.mesh.CombineMeshes(sliceParts);

                            ChunkMeshSlice slice = new ChunkMeshSlice();
                            slice.z = geoData.chunksForSlices[k].id;
                            slice.verts = finishedfilter.mesh.vertices;
                            slice.tris = finishedfilter.mesh.triangles;
                            loadedGeo.chunkSlices.Add(slice);
                            GameObject.Destroy(finishedfilter.gameObject);
                            GameObject.Destroy(combining);
                        }*/

                        // DECORATOR CUBES
                        loadedGeo.decoratorCubes = new Il2CppSystem.Collections.Generic.List<OscillatingObjectData>(0);
                        foreach (OscillatingObjectData dat in geoData.decCubes)
                        {
                            loadedGeo.decoratorCubes.Add(dat);
                        }
                    }

                    __result = loadedGeo;
                }
            }
        }

        public static HarmonyLib.Harmony harmony;
        public static bool allPatched = false;
        public static System.Collections.IEnumerator PatchVariablesAndMethods()
        {
            var GameMap_geoSet_orig = typeof(GameMap).GetProperty("GeoSet").GetGetMethod();
            var GameMap_geoSet_postfix = typeof(GameMap_geoSet).GetMethod("Postfix");
            harmony.Patch(GameMap_geoSet_orig, null, new HarmonyMethod(GameMap_geoSet_postfix));

            yield return null;

            var GameManager_gameTime_orig = typeof(GameManager).GetProperty("GameTime").GetGetMethod();
            var GameManager_gameTime_postfix = typeof(GameManager_GameTimeField).GetMethod("Postfix");
            harmony.Patch(GameManager_gameTime_orig, null, new HarmonyMethod(GameManager_gameTime_postfix));

            yield return null;

            var GameManager_gameTimeFunc_orig = typeof(GameManager).GetMethod("get_GameTime");
            harmony.Patch(GameManager_gameTimeFunc_orig, null, new HarmonyMethod(GameManager_gameTime_postfix));

            yield return null;

            var GameManager_gameProg_orig = typeof(GameManager).GetMethod("get_GameProgress");
            var GameManager_gameProg_postfix = typeof(GameManager_GameProgress).GetMethod("Postfix");
            harmony.Patch(GameManager_gameProg_orig, null, new HarmonyMethod(GameManager_gameProg_postfix));

            allPatched = true;
        }


        public static System.Collections.IEnumerator UnPatchVariablesAndMethods()
        {
            var GameMap_geoSet_orig = typeof(GameMap).GetProperty("GeoSet").GetGetMethod();
            var GameMap_geoSet_postfix = typeof(GameMap_geoSet).GetMethod("Postfix");
            harmony.Unpatch(GameMap_geoSet_orig, GameMap_geoSet_postfix);

            yield return null;

            var GameManager_gameTime_orig = typeof(GameManager).GetProperty("GameTime").GetGetMethod();
            var GameManager_gameTime_postfix = typeof(GameManager_GameTimeField).GetMethod("Postfix");
            harmony.Unpatch(GameManager_gameTime_orig, GameManager_gameTime_postfix);

            yield return null;

            var GameManager_gameTimeFunc_orig = typeof(GameManager).GetMethod("get_GameTime");
            harmony.Unpatch(GameManager_gameTimeFunc_orig, GameManager_gameTime_postfix);

            yield return null;

            var GameManager_gameProg_orig = typeof(GameManager).GetMethod("get_GameProgress");
            var GameManager_gameProg_postfix = typeof(GameManager_GameProgress).GetMethod("Postfix");
            harmony.Unpatch(GameManager_gameProg_orig, GameManager_gameProg_postfix);

            allPatched = false;
        }


        public static class GameManager_GameTimeField
        {
            public static void Postfix(ref float __result)
            {
                if (customLevel != -1)
                {
                    __result = customAudio.time;
                }
            }
        }

        //[HarmonyPatch(typeof(GameManager), "get_GameTime", new System.Type[0])]
        public static class GameManager_GameTime
        {
            public static void Postfix(ref float __result)
            {
                if (customLevel != -1)
                {
                    __result = customAudio.time;
                }
            }
        }

        //[HarmonyPatch(typeof(GameManager), "get_GameProgress", new System.Type[0])]
        static float gameProgress = 0;
        public static class GameManager_GameProgress
        {
            public static void Postfix(GameManager __instance, ref float __result)
            {
                if (customLevel != -1)
                {
                    __result = customAudio.time / gameLength;
                }
            }
        }
    }
}