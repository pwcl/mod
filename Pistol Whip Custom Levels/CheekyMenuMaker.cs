﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using AK.Wwise;
using UnityEngine.Events;
using UnityEngine.UI;
using UnhollowerRuntimeLib;
using MelonLoader;
using TMPro;
using System.IO;
using System.Collections;

namespace Pistol_Whip_Custom_Levels
{
    class CheekyMenuMaker : MonoBehaviour
    {
        public CheekyMenuMaker(IntPtr ptr) : base(ptr) { }

        //public AssetBundle depends;
        bool hasRun = false;
        int maxScroll = 0;
        int currentScroll = 0;
        GameObject contentObject;

        void OnEnable()
        {
            if (gameObject.name == "SceneInventoryLMAO" && !hasRun)
            {
                MelonLogger.Msg("running cmm");
                hasRun = true;

                contentObject = GameObject.Find("Container");
                //contentObject.tag = "Custom";
                GameObject posterPrefab = CustomLevels.posterPrefab;

                /*SongPanelUIController spC = posterPrefab.GetComponent<SongPanelUIController>();
                spC.songSwitch = new AK.Wwise.State();
                var refs = Resources.FindObjectsOfTypeAll<WwiseStateReference>();
                foreach (WwiseStateReference reference in refs) {
                    if (reference.objectName == "TheFall")
                    {
                        spC.songSwitch.ObjectReference = reference;
                    }
                }*/

                //SceneInventoryManager invManager = GetComponent<SceneInventoryManager>();
                //invManager.featureButtons[0].gameObject.transform.parent.position += new Vector3(0, 0, -0.00069f);

                MelonLogger.Msg(Directory.GetDirectories("./Custom Levels/").Length);
                for (int i = 0; i < Directory.GetDirectories("./Custom Levels/").Length; i++)
                {
                    GameObject panel = GameObject.Instantiate(posterPrefab);
                    panel.name = "custom " + i;
                    byte[] imageBytes = File.ReadAllBytes(Directory.GetDirectories("./Custom Levels/")[i] + "/poster.png");
                    Texture2D tex = new Texture2D(2, 2);
                    Il2CppImageConversionManager.LoadImage(tex, imageBytes);
                    panel.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                    //panel.GetComponent<CHUI_ImgLinkButton>().selectionIconImg[0].sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

                    SongPanelUIController uIController = panel.GetComponent<SongPanelUIController>();
                    uIController.BaseName = "custom " + i;


                    panel.transform.SetParent(contentObject.transform, false);
                    panel.AddComponent<ChangeSong>();
                }

                /*GameObject imagePlane = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    imagePlane.transform.parent = panel.transform.GetChild(0);
                    imagePlane.transform.localScale = new Vector3(0.7f, 1.1f, 0.00001f);
                    //imagePlane.transform.localPosition += new Vector3(3.93f, -1.91f, 0);
                    imagePlane.GetComponent<MeshRenderer>().material = new Material(Shader.Find("UI/Default"));
                    imagePlane.GetComponent<MeshRenderer>().material.renderQueue = 3004;
                    //imagePlane.GetComponent<MeshRenderer>().material.color = UnityEngine.Random.ColorHSV();
                    imagePlane.GetComponent<MeshRenderer>().material.mainTexture = tex;
                    imagePlane.GetComponent<MeshRenderer>().material.SetTextureScale("_MainTex", new Vector2(-1, -1));*/

                /*GameObject bottomMask = GameObject.CreatePrimitive(PrimitiveType.Cube);
                bottomMask.transform.position = new Vector3(0, -1.85f, 5.03f);
                bottomMask.transform.localScale = new Vector3(10, 5, 0.00001f);
                bottomMask.GetComponent<MeshRenderer>().material = new Material(depends.LoadAsset("assets/maskshader.shader").Cast<Shader>());
                bottomMask.name = "Bottom Mask";
                bottomMask.transform.parent = contentObject.transform.parent;

                GameObject topMask = GameObject.CreatePrimitive(PrimitiveType.Cube);
                topMask.transform.position = new Vector3(0, 5.55f, 5.03f);
                topMask.transform.localScale = new Vector3(10, 5, 0.00001f);
                topMask.GetComponent<MeshRenderer>().material = new Material(depends.LoadAsset("assets/maskshader.shader").Cast<Shader>());
                topMask.name = "Top Mask";
                topMask.transform.parent = contentObject.transform.parent;*/


                /*GameObject playButton = GameObject.Find("PlayButton");
                
                GameObject scrollUP = GameObject.Instantiate(playButton);
                scrollUP.name = "Scroll Up Button";
                scrollUP.transform.GetChild(0).gameObject.active = true;
                scrollUP.transform.rotation = Quaternion.Euler(0, 0, 90);
                scrollUP.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
                scrollUP.transform.position = new Vector3(2.85f, 2.1f, 4.5f);
                scrollUP.transform.parent = contentObject.transform.parent;
                UnityEventTrigger upClick = scrollUP.transform.GetChild(0).GetChild(1).GetChild(3).GetComponent<UnityEventTrigger>();
                upClick.Event = new UnityEvent();
                upClick.Event.AddListener(new Action(ScrollUP));

                GameObject scrollDOWN = GameObject.Instantiate(playButton);
                scrollDOWN.name = "Scroll Down Button";
                scrollDOWN.transform.GetChild(0).gameObject.active = true;
                scrollDOWN.transform.rotation = Quaternion.Euler(0, 0, -90);
                scrollDOWN.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
                scrollDOWN.transform.position = new Vector3(2.85f, 1.60f, 4.5f);
                scrollDOWN.transform.parent = contentObject.transform.parent;
                UnityEventTrigger downClick = scrollDOWN.transform.GetChild(0).GetChild(1).GetChild(3).GetComponent<UnityEventTrigger>();
                downClick.Event = new UnityEvent();
                downClick.Event.AddListener(new Action(ScrollDOWN));

                int cls = Directory.GetDirectories("./Custom Levels/").Length;
                if (cls > 10)
                {
                    MelonLogger.Msg("" + ((cls / 5f) % 2));
                    if (Mathf.CeilToInt(cls / 5f) % 2 == 1)
                    {
                        contentObject.transform.localPosition -= new Vector3(0, 0.6f, 0);
                        contentObject.transform.localPosition -= new Vector3(0, 1.2f * Mathf.CeilToInt((cls/5f)-4), 0);
                        MelonLogger.Msg("odd result " + (Mathf.CeilToInt(cls / 5f) - 2));
                    } 
                    else
                    {
                        contentObject.transform.localPosition -= new Vector3(0, 1.2f * Mathf.CeilToInt((cls / 5f) - 3), 0);
                        MelonLogger.Msg("even result " + (Mathf.CeilToInt(cls / 5f) - 4));
                    }
                }
                maxScroll = Mathf.CeilToInt(Directory.GetDirectories("./Custom Levels/").Length/10);*/

                //CustomLevels.propMat = GameObject.Find("screen").GetComponent<MeshRenderer>().material;

                MelonLogger.Msg("Made Menus");
            } else
            {

                //depends = AssetBundle.LoadFromFile("./Mods/cldependencies");
                //var posterpref = depends.LoadAsset("assets/posterprefab.prefab");
                //GameObject imagePlane = GameObject.Instantiate(posterpref).Cast<GameObject>();
                //ChangeShader ics = imagePlane.AddComponent<Pistol_Whip_Custom_Levels.ChangeShader>();
                //ics.posterIndex = 15;


            }
        }

        void ScrollUP()
        {
            if (currentScroll > 0)
            {
                contentObject.transform.Translate(new Vector3(0, -2.4f, 0));
                GameObject.Find("Custom Content(Clone)").transform.GetChild(0).transform.Translate(new Vector3(0, -2.4f, 0));
                currentScroll--;
                Debug.Log("Scroll up");
                MelonLogger.Msg("Scroll up");
            }
            else
            {
                Debug.Log("Can't scroll up");
                MelonLogger.Msg("Can't scroll up");
            }
        }

        void ScrollDOWN()
        {
            if (currentScroll < maxScroll)
            {
                contentObject.transform.Translate(new Vector3(0, 2.4f, 0));
                GameObject.Find("Custom Content(Clone)").transform.GetChild(0).transform.Translate(new Vector3(0, 2.4f, 0));
                currentScroll++;
                Debug.Log("Scroll down");
                MelonLogger.Msg("Scroll down");
            } 
            else
            {
                Debug.Log("Can't scroll down");
                MelonLogger.Msg("Can't scroll down");
            }
        }
    }
}
