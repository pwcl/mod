﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnhollowerRuntimeLib;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MelonLoader;

namespace Pistol_Whip_Custom_Levels
{
    class ChangeSong : MonoBehaviour
    {
        public ChangeSong(IntPtr ptr) : base(ptr) { }

        public int levelIndex;
        SongInfoUI info = CustomLevels.infoUI;
        GameManager manager = CustomLevels.manager;
        DifficultySelector difficultySelector = CustomLevels.difficultySelector;
        SceneDetailManager detailManager = CustomLevels.detailManager;
        GameManager gameManager = CustomLevels.gameManager;
        AudioSource customAudio = CustomLevels.customAudio;
        public float previewTime = 0;
        public Sprite songImage = new Sprite();
        public Difficulty[] playableDiffs;
        public float songLength;
        public string[] niceApiNames;

        void Start()
        {
            UnityEventTrigger clickTrigger = transform.GetChild(1).GetChild(3).GetComponent<UnityEventTrigger>();
            //clickTrigger.Event.AddListener(new Action(SetLevel));

            UnityEventTrigger hoverTrigger = transform.GetChild(1).GetChild(0).GetComponent<UnityEventTrigger>();
            hoverTrigger.Event.AddListener(new Action(CustomHover));
        }

        void OnEnable()
        {
            transform.GetChild(0).GetChild(1).gameObject.SetActive(false);

            /*byte[] imageBytes;
            string path = Directory.GetDirectories("./Custom Levels/")[transform.GetSiblingIndex()] + "/poster.png";
            if (File.Exists(path))
            {
                imageBytes = File.ReadAllBytes(path);
            }
            else
            {
                Bitmap bitmap = new Bitmap(CustomLevels.thisAssembly.GetManifestResourceStream("Pistol_Whip_Custom_Levels.missing.png"));
                ImageConverter converter = new ImageConverter();
                imageBytes = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
            }
            Texture2D tex = new Texture2D(2, 2);
            Il2CppImageConversionManager.LoadImage(tex, imageBytes);
            GetComponent<UnityEngine.UI.Image>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);*/
        }

        void CustomHover()
        {
            CustomLevels.currentLevelHover = transform.GetSiblingIndex();
            //GameManager.Instance.currentDifficulty = playableDiffs[0];
        }
    }
}
