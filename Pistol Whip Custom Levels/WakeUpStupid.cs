﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnhollowerRuntimeLib;
using MelonLoader;

namespace Pistol_Whip_Custom_Levels
{
    class WakeUpStupid : MonoBehaviour
    {
        public WakeUpStupid(IntPtr ptr) : base(ptr) { }


        void Start()
        {
            if (gameObject.name == "Custom Content")
            {
                transform.position = new Vector3(420f, 1.925f, 5.039f);
            } else
            {
                transform.position = new Vector3(0f, 1.925f, 5.039f);
                CustomLevels.LoadPosters();
            }
        }

        void Update()
        {
            if (CustomLevels.detailManager != null && CustomLevels.customPosterScroll != null)
            {
                CustomLevels.customPosterScroll.gameObject.SetActive(!CustomLevels.detailManager.gameObject.active);
            }
        }

    }
}
