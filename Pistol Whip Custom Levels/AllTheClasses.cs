﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;


namespace Pistol_Whip_Custom_Levels
{
    public class Geoset
    {
        public float version;
        public ChunkSlice[] chunksForSlices;
        public Chunk[] chunks;
        public OscillatingObjectData[] decCubes; 
        public StaticProp[] staticProps;
        public DynamProp[] dynamicProps;
        public string[] spropNames;
        public string[] dpropNames;
    }
    public class Chunk
    {
        public Vector3Int id;
        public int[] tris;
        public Vector3[] verts;
    }
    public class ChunkSlice
    {
        public int[] chunks;
        public int id;
    }
    public class StaticProp
    {
        public Vector3 pos;
        public Vector3 rot;
        public Vector3 scale;
        public string name;
        public string path;
    }
    public class DynamProp
    {
        public Vector3 pos;
        public Vector3 rot;
        public Vector3 scale;
        public Vector3 Start;
        public Vector3 End;
        public string name;
    }

    public class BeatFile
    {
        public float version;
        public List<CustomBeat> beatData = new List<CustomBeat>(0);
        public List<float> beatTimes = new List<float>(0);
    }

    public class CustomBeat
    {
        public float time;
        public List<CustomTargetData> targets = new List<CustomTargetData>(0);
        public List<CustomObstacleData> obstacles = new List<CustomObstacleData>(0);
        public List<CustomDynamicObstacleData> dynamicObstacles = new List<CustomDynamicObstacleData>(0);
    }

    public class CustomTargetData
    {
        public string type;
        public Distance distance;
        public Vector2Int placement;
        public WorldPoint enemyOffset;
        public List<CustomEnemyAction> enemySequence = new List<CustomEnemyAction>(0);
        public bool bonusEnemy;
        public bool shielded;
        public bool noGround;
        public bool noCarve;
        public float playerActionLerp;

        public enum Distance
        {
            Near = 0,
            Middle = 1,
            Far = 2
        }

        public enum EnemySet1
        {
            Enemy_Normal = 0,
            Enemy_Tough = 1,
            Enemy_ChuckNorris = 2,
            Enemy_2089_Minigun = 4,
            Enemy_Shield = 5,
            Enemy_2089_SentyGun = 7,
            Enemy_HorseRider = 8
        }

        public enum EnemySet2
        {
            Enemy_2089_1Shot = 0,
            Enemy_2089_2Shot = 1,
            Enemy_2089_4Shot = 2,
            Enemy_2089_Minigun = 4,
            Enemy_2089_Shield = 5,
            Enemy_2089_SentyGun = 7,
            Enemy_AP2_HorseRider = 8
        }

        public enum EnemySet3
        {
            Enemy_AP2_1Shot = 0,
            Enemy_AP2_2Shot = 1,
            Enemy_AP2_4Shot = 2,
            Enemy_2089_Minigun = 4,
            Enemy_Shield = 5,
            Enemy_2089_SentyGun = 7,
            Enemy_AP2_HorseRider = 8
        }
    }

    public class CustomEnemyAction
    {
        public ActionType enemyAction;
        public EnemyActionProperties properties = new EnemyActionProperties();

        public enum ActionType
        {
            EnemyActionWait = 0,
            EnemyActionMove = 1,
            EnemyActionAimStart = 2,
            EnemyActionAimAndFire = 3,
            EnemyActionFire = 4,
            EnemyActionAimStop = 5,
            //EnemyActionAnimation = 6,
            EnemyActionStopFiring = 7,
            EnemyActionDespawn = 8,
            EnemyActionFacePlayerStart = 9,
            EnemyActionFacePlayerStop = 10,
            EnemyActionAimWithoutFacing = 11
            //EnemyActionStrafe = 12
            //EnemyActionInstant = 13
        }
    }

    public class EnemyActionProperties
    {
        public float duration = 0;
        public WorldPoint destination;
        public bool isOverrideAnimationSpeed;
        public float animationSpeedOverride;
        public EnemyActionMove.Facing facing;
        public float fireTime;
        public bool stopFacingOnExit;
        public bool stopLookingOnExit;
        public EnemyActions action;
        public int triggerHash; // Need more info
    }

    public enum EnemyActions
    {
        tooLazyDoLater = 0
    }

    public class CustomObstacleData
    {
        public ObstaclePlacement placement;
        public ObstacleType type;

        public enum ObstaclePlacement
        {
            EvenMoreLeft = -3,
            FarLeft = -2,
            Left = -1,
            Center = 0,
            Right = 1,
            FarRight = 2,
            EvenMoreRight = 3
        }

        public enum ObstacleType
        {
            Sidestep = 0,
            LimboTall = 1,
            LimboShort = 2,
            Wall = 3
        }
    }

    public class CustomDynamicObstacleData
    {

    }
    public class WorldPointMock
    {
        public Vector3 position;
        public Quaternion rotation;
    }

    public class LevelInfo
    {
        public float version;
        public string sceneDisplayName;
        public string songDisplayName;
        public string songLocation;
        public float songLength;
        public string description;
        public string songArtist;
        public string mapper;
        public float tempo;
        public float previewTime;
        public List<string> maps = new List<string>(0);
        public ObstacleSets obstacleSet;
        public MaterialPropertiesSet materialPropertiesSet;
        public EnemySets enemySet;
        public float moveSpeed = 3;
        public float timingWindowSize = 150f;
        public menuColor customMainMenuColor;
    }

    public class menuColor
    {
        public Color Main;
        public Color Fog;
        public Color Glow;
        public Color Enemy;
    }

        public enum EnemySets
    {
        Normal = 0,
        Robots = 1,
        Outlaws = 2
    }

    public enum ObstacleSets
    {
        Normal = 0,
        Spooky = 1,
        Pipes = 2,
        Rocks = 3,
        AirDrop = 4,
        Colony = 5,
        Tower = 6,
        EarthCracker = 7,
        Crates = 8,
        Train = 9
    }

    public enum MaterialPropertiesSet
    {
        Default = 0,
        Heartbreaker = 1,
        AlienPlanet_2089 = 2,
        Arbiter_2089 = 3,
        RobotFacilities_2089 = 4,
        StrangeCreatures_2089 = 5,
        Cave_AP2 = 6,
        Desert_AP2 = 7,
        WesternTown = 8,
        OldWestTrain = 9
    }

    public enum MoveMode
    {
        Moving = 0,
        Stationary = 1
    }

    public class KoreographyBaseC
    {
        public int mSampleRate = 44100;
        public TempoSectionDefC[] mTempoSections;
        public KoreographyTrackBaseC[] mTracks;
    }

    public class TempoSectionDefC
    {
        public string sectionName;
        public int startSample;
        public double samplesPerBeat;
        public int beatsPerMeasure;
        public bool bStartNewMeasure;
    }

    public class KoreographyTrackBaseC
    {
        public string mEventID;
        public KoreographyEventC[] mEventList;
    }

    public class KoreographyEventC
    {
        public int mStartSample;
        public int mEndSample;
    }

    public enum SectionType
    {
        Beat = 1,
        NoBeat = 2
    }

    public class TSection
    {
        public string name = "";
        public int start;
        public int end;
        public float bpm;
        public bool beat;
        public float colorHue;
        public float colorSaturation;
        public float colorValue;
        public Color fogColor;
        public Color glowColor;
        public Color mainColor;
        public Color enemyColor;
        public bool customEnemyColor = false;
    }

    public class DumbPoint
    {
        public float start;
        public float end;
        public ColorData colors;
    }

    public class someColors
    {
        public Vector3 mainColor;
        public Vector3 fogColor;
        public Vector3 glowColor;
        public bool customEnemyColor;
        public Vector3 storedEnemyColor;
    }

    public class Tdata
    {
        public TSection[] sections;
        public DumbPoint[] colors;
    }

    public class GivoUtils
    {
        public static MeshFilter[] GetChildMeshFilters(Transform parentObject)
        {
            List<MeshFilter> meshFilters = new List<MeshFilter>(0);

            for (int i = 0; i < parentObject.childCount; i++)
            {
                if (parentObject.GetChild(i).gameObject.GetComponent<MeshFilter>() == null)
                {
                    foreach (MeshFilter filter in GetChildMeshFilters(parentObject.GetChild(i)))
                    {
                        meshFilters.Add(filter);
                    }
                }
                else
                {
                    meshFilters.Add(parentObject.GetChild(i).gameObject.GetComponent<MeshFilter>());
                    foreach (MeshFilter filter in GetChildMeshFilters(parentObject.GetChild(i)))
                    {
                        meshFilters.Add(filter);
                    }
                }
            }

            return meshFilters.ToArray();
        }

        public static MeshRenderer[] GetChildMeshRenderers(Transform parentObject)
        {
            List<MeshRenderer> meshRenderers = new List<MeshRenderer>(0);

            for (int i = 0; i < parentObject.childCount; i++)
            {
                if (parentObject.GetChild(i).gameObject.GetComponent<MeshRenderer>() == null)
                {
                    foreach (MeshRenderer renderer in GetChildMeshRenderers(parentObject.GetChild(i)))
                    {
                        meshRenderers.Add(renderer);
                    }
                } else
                {
                    meshRenderers.Add(parentObject.GetChild(i).gameObject.GetComponent<MeshRenderer>());
                    foreach (MeshRenderer renderer in GetChildMeshRenderers(parentObject.GetChild(i)))
                    {
                        meshRenderers.Add(renderer);
                    }
                }
            }

            return meshRenderers.ToArray();
        }
    }
}
